#include<cstdio>
#include<vector>

using namespace std;
typedef pair<int,int> pii;

int main()
{
    int n,m,x,y;
    scanf("%d %d %d %d",&n,&m,&x,&y);
    int A[100001],B[100001];
    for(int i=0;i<n;i++)
        scanf("%d",&A[i]);
    for(int i=0;i<m;i++)
        scanf("%d",&B[i]);
    vector<pii> ans;
    int i=0,j=0;
    while(i<n and j<m)
    {
        if(A[i]-x<=B[j] and B[j]<=A[i]+y)
        {
            ans.push_back(pii(i+1,j+1));
            i++,j++;
        }
        else if(A[i]-x>B[j])
            j++;
        else
            i++;
    }
    printf("%d\n",ans.size());
    for(auto x:ans)
        printf("%d %d\n",x.first,x.second);
}
