#include <iostream>
#include <vector>
#include <cmath>
#define MAXN 100001

using namespace std;

typedef long long type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  double norm() const {
    return sqrt(norm2());
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;

using namespace std;

pt A[MAXN], B[MAXN];

double calc(int id, int jd) {
  return (B[jd] - A[id]).norm() + A[id].norm();
}

int main() {
  int n, m, a, b, y;
  cin >> n >> m >> a >> b;
  for (int i = 0; i < n; ++i) {
    cin >> y;
    A[i] = pt(a, y);
  }
  for (int i = 0; i < m; ++i) {
    cin >> y;
    B[i] = pt(b, y);
  }
  double ans = 1e300, tmp;
  int id, jd, tjd, l;
  for (int i = 0; i < m; ++i) {
    cin >> l;
    int lo = 0, hi = n, m1, m2;
    while (hi - lo > 2) {
      m1 = (2*lo+hi) / 3, m2 = (lo+2*hi) / 3;
      if (calc(m2, i) > calc(m1, i))
        hi = m2;
      else
        lo = m1;
      m1 = (2*lo+hi) / 3, m2 = (lo+2*hi) / 3;
    }
    tmp = 1e300;
    for (int j = lo; j < hi; ++j)
      if (tmp > calc(j, i)) {
        tmp = calc(j, i);
        tjd = j;
      }
    if (tmp + l < ans) {
      ans = tmp + l;
      jd = tjd;
      id = i;
    }
  }
  cout << jd+1 << " " << id+1 << endl;
}
