#include <iostream>

using namespace std;

int main() {
  string s;
  int k, n;
  cin >> s >> k;
  n = s.size();
  for (int p = 0; p < n and k; ++p) {
    int id = p;
    for (int i = p + 1; i < n and i - p <= k; ++i)
      if (s[id] < s[i])
        id = i;
    char tmp = s[id];
    k -= id - p;
    for (int i = id - 1; i >= p; --i)
      s[i+1] = s[i];
    s[p] = tmp;
  }
  cout << s << endl;
}
