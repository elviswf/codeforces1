#include<iostream>
#include<vector>

using namespace std;

int main() {
    int n;
    cin>>n;
    vector<int> A(n);
    for(int i=0; i<n; i++)
        cin>>A[i];
    int este, ans = 0;
    for(int i=0; i<n; i++)
        for(int j=i; j<n; j++) {
            este = 0;
            for(int k=i; k<=j; k++)
                este ^= A[k];
            ans = max(este, ans);
        }
    cout<<ans<<endl;
}
