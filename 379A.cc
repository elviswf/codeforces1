#include <iostream>

using namespace std;

int main() {
  int a, b, r = 0, div;
  cin >> a >> b;
  r += a;
  int ans = a;
  while (r >= b) {
    div = r / b;
    r = r % b + div;
    ans += div;
  }
  cout << ans << endl;
}
