n = int(raw_input())
A = [int(x) for x in raw_input().split()]

A.sort()

i = 1
k = 0
B = [1]
while i<len(A):
    if A[i]==A[i-1]:
        B[k] += 1
    else:
        k +=1
        B.append(1)
    i += 1
B = [x/2 for x in B]

print sum(B)/2
