#include<cstdio>
#include<iostream>
#include<vector>

using namespace std;

vector<vector<int> > adj;
vector<int> dep;

void dfs(int u)
{
	for(int v:adj[u])
	{
		dep[v]=dep[u]+1;
		dfs(v);
	}
}

int main()
{
	int n,t;
	scanf("%d",&n);
	adj.resize(n);
	vector<int> p(n);
	dep.assign(n,1);
	for(int i=0;i<n;i++)
	{
		scanf("%d",&t);
		if(t!=-1)
		{
			t--;
			adj[t].push_back(i);
			p[i]=t;
		}
		else
			p[i]=-1;
	}
	for(int i=0;i<n;i++)
		if(p[i]==-1)
			dfs(i);
	int rpta=0;
	for(int x:dep)
		rpta=max(rpta,x);
	printf("%d\n",rpta);
}
