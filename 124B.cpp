#include<iostream>
#include<vector>
#include<algorithm>
#include<cstdio>

using namespace std;

bool order(string a,string b)
{
	int aa,bb;
	sscanf(a.c_str(),"%d",&aa);
	sscanf(b.c_str(),"%d",&bb);
	return aa<bb;
}

int main()
{
	int n,k,aa,bb,r=1<<30;
	cin>>n>>k;
	vector<int> p(k);
	vector<string> A(n);
	for(int i=0;i<k;i++)
		p[i]=i;
	for(int i=0;i<n;i++)
		cin>>A[i];
	do
	{
		vector<string> B(n);
		for(int i=0;i<n;i++)
			for(int j=0;j<k;j++)
				B[i].push_back(A[i][p[j]]);
		sort(B.begin(),B.end(),order);
		sscanf(B[0].c_str(),"%d",&aa);
		sscanf(B[n-1].c_str(),"%d",&bb);
		if(bb-aa<r)
		{
			r=bb-aa;
		}
	} while(next_permutation(p.begin(),p.end()));
	cout<<r<<endl;
}
