n = int(raw_input())
A = [int(x) for x in raw_input().split()]
B = [0] * n
for i,x in enumerate(A):
    B[x-1] = i + 1

for i in range(n-1):
    print B[i],
print B[-1]
