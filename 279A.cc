#include <iostream>
#include <cmath>

using namespace std;

int main() {
  int X, Y, ans = 0, x = 0, y = 0;
  cin >> X >> Y;
  int delta = 1, mv = 1, tmp = 1, dir = 1;
  bool coord = 0;
  while (x != X or y != Y) {
    if (tmp == 0) {
      if (mv == 0) {
        delta ++;
        mv = 2;
        dir = -dir;
      }
      coord = not coord;
      tmp = delta;
      ans ++;
      mv --;
    }
    if (not coord)
      x += dir, tmp --;
    else
      y += dir, tmp --;
  }
  cout << ans << endl;
}
