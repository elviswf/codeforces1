#include <iostream>
#include <cstdio>
#include <algorithm>
#define MAXN 100005

using namespace std;

template<typename T>
struct p2d {
  T x, y;
  p2d() {}
  p2d(const T &x, const T &y) : x(x), y(y) {}
  inline p2d operator - (const p2d &a) {
    return p2d(x-a.x, y-a.y);
  }
  inline T operator % (const p2d &a) {
    return x * a.y - y * a.x;
  }
};
typedef long long ll;
const ll INF = 1LL << 62;
typedef p2d<ll> pt;

pt T[MAXN];
int begin, end;
inline void add(ll x, ll y) { //increasing x
  pt p(x, y);
  while (end - begin >= 2 and (p-T[end-1]) % (p-T[end-2]) >= 0)
    end--;
  T[end++] = p;
}
inline ll eval(int i, ll x) {
  return T[i].x * x + T[i].y;
}
inline void remove(ll x) { //only if decreasing queries
  while (end - begin >= 2 and eval(begin, x) > eval(begin+1, x))
    begin++;
}

ll pos[MAXN], P[MAXN], A[MAXN];
ll DP[MAXN], DPa[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m, p, x, t;
  scanf("%d %d %d", &n, &m, &p);
  pos[0] = 0;
  for (int i = 1; i < n; i++) {
    scanf("%d", &x);
    pos[i] = pos[i-1] + x;
  }
  for (int i = 0; i < m; i++) {
    scanf("%d %d", &x, &t);
    P[i] = t - pos[x-1];
  }
  sort(P, P + m);
  A[0] = P[0];
  for (int i = 1; i < m; i++)
    A[i] = A[i-1] + P[i];
  for (int k = 0; k <= p; k++) {
    begin = end = 0;
    for (int i = n; i >= 0; i--) {
      ll &res = DP[i];
      if (i == m) {
        res = k == 0 ? 0 : INF;
      } else {
        if (k and DPa[i+1] != INF)
          add(-P[i], P[i]*(i+1)-A[i]+DPa[i+1]);
        remove(i);
        res = end > begin ? eval(begin, i)+(i?A[i-1]:0): INF;
        //res = INF;
        /*
        int lo = 0, hi = end, m1, m2;
        while (hi - lo > 4) {
          m1 = (2*lo+hi) / 3, m2 = (lo+2*hi)/3;
          if (eval(m1, i) < eval(m2, i))
            hi = m2;
          else
            lo = m1;
        }
        for (int j = lo; j < hi; j++)
          res = min(res,
              eval(j, i) + (i?A[i-1]:0));
        */
      }
    }
    copy(DP, DP+m+2, DPa);
  }
  cout << DP[0] << endl;
}
