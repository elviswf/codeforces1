#include <iostream>

using namespace std;
typedef pair<int, int> pii;
pii P[1001];

bool verify(int n, const pii &t) {
    for (int i = 0; i < n; ++i) {
        if ((P[i].first < t.first and P[i].second > t.first and P[i].second < t.second)
                or (t.first < P[i].first and t.second > P[i].first and t.second < P[i].second))
            return true;
    }
    return false;
}

int main() {
    int n;
    cin >> n;
    int x, y;
    cin >> x;
    bool valid = false;
    for (int i = 1; i < n; ++i) {
        cin >> y;
        pii t(min(x, y), max(x, y));
        valid |= verify(i-1, t);
        P[i-1] = t;
        x = y;
    }
    cout << (valid ? "yes" : "no") << endl;
}
