#include <cstdio>
#include <algorithm>

using namespace std;

#define MAXN 524290
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
int st[MAXN];
int pos[MAXN>>1];
int n;

void stupdate(int i, int up) {
  int idx = pos[i], L, R;
  st[idx] = up;
  idx = (idx-1) >> 1;
  while (idx) {
    L = left(idx), R = L + 1;
    st[idx] = max(st[L], st[R]);
    idx = (idx-1) >> 1;
  }
}

int cmp;

bool stq(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    return st[idx] > cmp;
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return stq(i, j, L, l, mid) or stq(i, j, R, mid+1, r);
}

void stbuild(int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l == r) {
    pos[l] = idx;
    return;
  }
  int mid = center(l, r);
  stbuild(L, l, mid);
  stbuild(R, mid+1, r);
}

typedef pair<int, int> pii;
typedef pair<int, pii> piii;
pii P[MAXN >> 1];
piii Q[MAXN >> 1];

int main() {
  int q;
  scanf("%d", &q);
  char op, s[10];
  for (int i = 0; i < q; i++) {
    scanf("%s %d %d", s, &Q[i].second.first, &Q[i].second.second);
    Q[i].first = 0;
    if (s[0] == 'f')
      Q[i].first = 1;
    else if (s[0] == 'r')
      Q[i].first = 2;
    P[i] = Q[i].second;
  }
  sort(P, P+q);
  n = unique(P, P+q) - P;
  stbuild();
  int pos;
  for (int i = 0; i < q; i++) {
    if (Q[i].first == 0) {
      pos = lower_bound(P, P+n, Q[i].second) - P;
      //printf("> %d %d\n", Q[i].second.first, Q[i].second.second);
      stupdate(pos, Q[i].second.second);
    } else if (Q[i].first == 1) {
      //binary_search
      int lo = lower_bound(P, P+n, pii(Q[i].second.first+1, -1))-P-1,
          hi = n, mid;
      cmp = Q[i].second.second;
      while (lo < hi - 1) {
        mid = (lo+hi)/2;
        //printf("# %d %d %d: %d\n", lo, hi, mid, stq(pos+1, mid));
        if (stq(lo+1, mid))
          hi = mid;
        else
          lo = mid;
      }
      if (hi == n)
        puts("-1");
      else
        printf("%d %d\n", P[hi].first, P[hi].second);
    } else {
      pos = lower_bound(P, P+n, Q[i].second) - P;
      stupdate(pos, 0);
      //printf("< %d\n", stq(0, n-1));
    }
  }
}

