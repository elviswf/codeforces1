#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long ll;

inline ll C(int n) {
    return 1LL*n*(n-1)/2;
}

int main() {
    ios_base::sync_with_stdio(false);
    int n, d;
    cin>>n>>d;
    vector<int> A(n);
    for(int i=0; i<n; i++)
        cin>>A[i];
    vector<int>:: iterator it = A.begin(), it2;
    ll ans = 0;
    for(int i=0; i<n-2; ++i, ++it) {
        it2 = upper_bound(it+2, A.end(), A[i]+d);
        ans += C(it2-it-1);
    }
    cout<<ans<<endl;
}
