#include <iostream>

using namespace std;

typedef long long ll;

#define MAXN 100001

ll A[MAXN], B[MAXN];
bool valid[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 0; i < n; ++i)
    cin >> B[i];
  ll ac = 0, bc = 0;
  // clockwise
  ll mini = (1LL << 62);
  for (int i = 0; i < n; ++i) {
    ac += A[i];
    bc += B[i];
    mini = min(mini, ac-bc);
  }
  for (int i = 0; i < n; ++i) {
    valid[i] = mini >= 0;
    mini -= A[i]-B[i];
  }
  // counter clockwise
  mini = (1LL << 62);
  ac = bc = 0;
  for (int i = 0; i < n; ++i) {
    ac += A[n-i-1];
    bc += B[(n-i-2+n)%n];
    mini = min(mini, ac-bc);
  }
  for (int i = 0; i < n; ++i) {
    valid[n-i-1] |= mini >= 0;
    mini -= A[n-i-1] - B[(n-i-2+n)%n];
  }
  bool first = true;
  int ans = 0;
  for (int i = 0; i < n; ++i)
    ans += valid[i];
  cout << ans << endl;
  for (int i = 0; i < n; ++i)
    if (valid[i]) {
      if (not first)
        cout<< " ";
      cout << i+1;
      first = false;
    }
  cout << endl;
}

