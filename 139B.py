from math import ceil
n=int(raw_input())
A=[]
for i in range(n):
	A.append([int(x) for x in raw_input().split()])
B=[]
m=int(raw_input())
for i in range(m):
	B.append([int(x) for x in raw_input().split()])
ans=0
for x in A:
	per=2*(x[0]+x[1])
	he=x[2]
	this=1<<30
	for y in B:
		if y[0]>=he:
			hor=ceil(float(per)/y[1])
			q=int(ceil(hor/(y[0]/he)))
			this=min(this,q*y[2])
	ans+=this
print ans

