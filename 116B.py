s=raw_input().split()
n=int(s[0])
m=int(s[1])

M=['']*n
for i in range(n):
	M[i]=raw_input()

dx=[-1,1,0,0]
dy=[0,0,-1,1]

def valid(i,j):
	return i>=0 and j>=0 and i<n and j<m

def hay(i,j):
	for k in range(4):
		ii=i+dx[k]
		jj=j+dy[k]
		if(valid(ii,jj) and M[ii][jj]=='P'):
			return 1
	return 0

r=0
for i in range(n):
	for j in range(m):
		if(M[i][j]=='W' and hay(i,j)):
			r+=1
print r
