#include <iostream>
#include <vector>
#define MOD 1000000007

using namespace std;
typedef long long ll;

template <typename T>
class Matrix {
  size_t n;
  vector<vector<T> > M;

  public:
  Matrix() {}

  Matrix(size_t n, const T &v = 0) :
    n(n), M(n, vector<T> (n, v)) {}

  vector<T>& operator [] (size_t i) {
    return M[i];
  }

  static Matrix identity(size_t n) {
    Matrix I(n);
    for (size_t i = 0; i < n; i++)
      I[i][i] = 1;
    return I;
  }

  Matrix operator * (Matrix &B) {
    Matrix A(n, 0);
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        for (int k = 0; k < n; k++) {
          A[i][j] += M[i][k] * B[k][j];
          if (A[i][j] >= MOD)
            A[i][j] %= MOD;
        }
    return A;
  }

  Matrix exp(ll e) {
    Matrix P = identity(n), A = *(this);
    while (e) {
      if (e & 1)
        P = A * P;
      e >>= 1;
      A = A * A;
    }
    return P;
  }
};

int main() {
  ll n;
  cin >> n;
  Matrix<ll> M(2, 1);
  M[0][0] = M[1][1] = 3;
  M = M.exp(n);
  cout << M[0][0] << endl;
}
