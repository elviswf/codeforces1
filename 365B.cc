#include <iostream>
#define MAXN 100001

using namespace std;

int A[MAXN], DP[MAXN];

int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  DP[n] = 0;
  int ans = 0;
  for (int i = n-1; i > 1; --i) {
    DP[i] = A[i] == A[i-1] + A[i-2] ? 1 + DP[i+1] : 0;
    ans = max(ans, DP[i]);
  }
  cout << min(n, ans + 2) << endl;
}
