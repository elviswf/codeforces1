#include<vector>
#include<cstdio>
#include<algorithm>

using namespace std;

int n;
vector<vector<int> > adj;
vector<bool> visit;
vector<int> tpsort;

void dfsvisit(int u)
{
	visit[u]=1;
	for(int v:adj[u])
		if(!visit[v])
			dfsvisit(v);
	tpsort.push_back(u);
}

void toposort()
{
	visit.assign(n,0);
	for(int u=0;u<n;u++)
		if(!visit[u])
			dfsvisit(u);
	reverse(tpsort.begin(),tpsort.end());
}

int main()
{
	scanf("%d",&n);
	adj.resize(n);
	int u,v;
	vector<vector<int> > nodos(n);
	for(int i=0;i<n*(n-1)/2-1;i++)
	{
		scanf("%d %d",&u,&v);
		u--,v--;
		adj[u].push_back(v);
		nodos[u].push_back(v);
		nodos[v].push_back(u);
	}
	toposort();
	int sum=0,falta;
	for(int u:tpsort)
		if(nodos[u].size()<n-1)
		{
			sum=0;
			for(int v:nodos[u])
				sum+=v;
			sum+=u;
			falta=(n-1)*n/2-sum;
			printf("%d %d\n",u+1,falta+1);
			return 0;
		}
}
