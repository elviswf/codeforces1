#include<cstdio>

int d,A[31],B[31],M[241][31],g[241][31];

bool dp(int ttime,int pos)
{
	if(M[ttime][pos]!=-1)
		return M[ttime][pos];
	//printf("#%d %d\n",ttime,pos);
	if(pos==d)
	{
		if(ttime>0)
			return M[ttime][pos]=false;
		else
			return M[ttime][pos]=true;
	}
	if(ttime<A[pos])
		return false;
	for(int i=A[pos];ttime-i>=0 and i<=B[pos];i++)
		if(dp(ttime-i,pos+1))
		{
			g[ttime][pos]=i;
			return M[ttime][pos]=true;
		}
	return M[ttime][pos]=false;
}

int main()
{
	int ttime;
	scanf("%d %d",&d,&ttime);
	for(int i=0;i<d;i++)
		scanf("%d %d",&A[i],&B[i]);
	for(int i=0;i<=ttime;i++)
		for(int j=0;j<=d;j++)
			M[i][j]=-1;
	if(dp(ttime,0))
	{
		puts("YES");
		for(int i=0;i<d-1;i++)
		{
			printf("%d ",g[ttime][i]);
			ttime-=g[ttime][i];
		}
		printf("%d\n",g[ttime][d-1]);
	}
	else
		puts("NO");
}
