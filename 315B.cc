#include <iostream>
#include <cstring>
#define MAXN 100003

using namespace std;

int A[MAXN], ac[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  memset(ac, 0, sizeof ac);
  for (int i = 0; i < n; i++)
    cin >> A[i];
  int au = 0;
  int op, v, x;
  while (m--) {
    cin >> op >> v;
    if (op == 1) {
      cin >> x;
      A[v-1] = x;
      ac[v-1] = au;
    } else {
      if (op == 2)
        au += v;
      else
        cout << A[v-1] + au - ac[v-1] << endl;
    }
  }
}
