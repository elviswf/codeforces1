#include <cstdio>
#include <algorithm>

using namespace std;

struct tri {
    int x, y, v;
    tri(int x, int y, int v) {
        this->x = x;
        this->y = y;
        this->v = v;
    }
    tri() {}
};
bool operator < (tri a, tri b) {
    return a.x<b.x;
}
const int H = 100, L=100000;

int main() {
    int h1, h2, N, n[2];
    n[0] = n[1] = 0;
    scanf("%d %d %d", &h1, &h2, &N);
    tri A[2][101];
    int v, a, b;
    char c;
    while(N--) {
        scanf("%d %c %d %d", &v, &c, &a, &b);
        if(c=='F')
            A[0][n[0]++] = tri(a, b, v);
        else
            A[1][n[1]++] = tri(a, b, v);
    }
    sort(A[0], A[0]+n[0]);
    sort(A[1], A[1]+n[1]);
    int ans = 0;
    for(int i=0; i<2; i++) {
        for(int j=0; j<n[i]; j++) {
            int points = 0, id = -1;
            double tg = 1.*L/(h1+2*H*j+h2), p = h1*tg;
            bool valid = true;
            for(int k=0; valid and k<=j; k++) {
                id ++;
                while(id<n[i] and (p<A[i][id].x or p>A[i][id].y))
                    id ++;
                if(id==n[i])
                    valid = false;
                points += A[i][id].v;
                p += 2*H*tg;
            }
            p = (h1+H)*tg;
            id = -1;
            for(int k=0; valid and k<j; k++) {
                id ++;
                while(id<n[1-i] and (p<A[1-i][id].x or p>A[1-i][id].y))
                    id ++;
                if(id==n[1-i])
                    valid = false;
                points += A[1-i][id].v;
                p += 2*H*tg;
            }
            if(valid)
                ans = max(ans, points);
        }
        h1 = H-h1;
        h2 = H-h2;
    }
    for(int i=0; i<2; i++) {
        for(int j=0; j<n[i]; j++) {
            int points = 0, id = -1;
            double tg = 1.*L/(h1+2*H*(j+1)-h2), p = h1*tg;
            bool valid = true;
            for(int k=0; valid and k<=j; k++) {
                id ++;
                while(id<n[i] and (p<A[i][id].x or p>A[i][id].y))
                    id ++;
                if(id==n[i])
                    valid = false;
                points += A[i][id].v;
                p += 2*H*tg;
            }
            p = (h1+H)*tg;
            id = -1;
            for(int k=0; valid and k<=j; k++) {
                id ++;
                while(id<n[1-i] and (p<A[1-i][id].x or p>A[1-i][id].y))
                    id ++;
                if(id==n[1-i])
                    valid = false;
                points += A[1-i][id].v;
                p += 2*H*tg;
            }
            if(valid)
                ans = max(ans, points);
        }
        h1 = H-h1;
        h2 = H-h2;
    }
    printf("%d\n", ans);
}
