#include <iostream>
#include <algorithm>

using namespace std;

typedef long long ll;
typedef pair<ll, bool> pib;

vector<pib> A;

string s;
int pos;

pib summand();
bool increment();
ll coefficient();

void expression() {
inicio:
  if (pos < 0)
    return;
  pib f = summand();
  if (pos < 0) {
    A.push_back(f);
    return;
  }
  if (s[pos] == '+')
    A.push_back(f);
  else
    A.push_back({-f.first, f.second});
  --pos;
  goto inicio;
}

pib summand() {
  bool x = increment();
  if (pos < 0 or s[pos] == '+' or s[pos] == '-')
    return {1, x};
  pos--;
  return {coefficient(), x};
}

bool increment() {
  bool ans = s[pos] == 'a'; // update first
  pos -= 3;
  return ans;
}

ll coefficient() {
  ll f = 1, ans = 0;
  while (pos >= 0 and s[pos] >= '0' and s[pos] <= '9') {
    ans += (s[pos--]-'0') * f;
    f *= 10;
  }
  return ans;
}

ll evaluate(ll a) {
  ll ans = 0;
  for (auto &x: A) {
    if (x.second)
      ans += x.first * ++a;
    else
      ans += x.first * a++;
  }
  return ans;
}

int main() {
  int ini;
  cin >> ini >> s;
  pos = s.size() - 1;
  expression();
  sort(A.begin(), A.end());
  cout << evaluate(ini) << endl;
}

