#include <iostream>

using namespace std;
int n;
int A[3][100002];

string s;

void cnt(int id, char c) {
  for (int i = 0; i < s.size(); ++i) {
    A[id][i+1] = A[id][i];
    A[id][i+1] += s[i] == c;
  }
}

bool solve(int x, int y) {
  if (y-x < 2)
    return true;
  int B[3];
  int maxi = 0;
  for (int i = 0; i < 3; ++i) {
    B[i] = A[i][y] - A[i][x-1];
    maxi = max(B[i], maxi);
  }
  for (int i = 0; i < 3; ++i)
    if (maxi - B[i] > 1)
      return false;
  return true;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> s;
  n = s.size();
  cnt(0, 'x');
  cnt(1, 'y');
  cnt(2, 'z');
  int m, x, y;
  cin >> m;
  while (m--) {
    cin >> x >> y;
    cout << (solve(x, y) ? "YES" : "NO") << endl;
  }
}
