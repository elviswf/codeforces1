#include <iostream>
#define MAXN 101

using namespace std;

char M[101][101];

int main() {
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    cin >> M[i];
    for (int j = 0; j < m; ++j)
      if (M[i][j] == '.')
        M[i][j] = (i ^ j) & 1 ? 'W' : 'B';
  }
  for (int i = 0; i < n; ++i)
    cout << M[i] << endl;
}
