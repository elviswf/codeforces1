#include <iostream>
#include <set>
#include <cstring>
#define MAXN 100005

using namespace std;

set<int> F[27];
string s;
int M[MAXN][27];

int DP(int id, int c) {
  if (id == s.size())
    return 0;
  int &ans = M[id][c];
  if (ans >= 0)
    return ans;
  ans = 1 + DP(id+1, c);
  if (F[s[id]].find(c) == F[s[id]].end())
    ans = min(ans, DP(id+1, s[id]));
  return ans;
}

int main() {
  cin >> s;
  for (int i = 0; i < s.size(); i++)
    s[i] -= 'a' - 1;
  int f;
  cin >> f;
  string p;
  while (f--) {
    cin >> p;
    p[0] -= 'a' - 1;
    p[1] -= 'a' - 1;
    F[p[0]].insert(p[1]);
    F[p[1]].insert(p[0]);
  }
  memset(M, -1, sizeof M);
  cout << DP(0, 0) << endl;
}
