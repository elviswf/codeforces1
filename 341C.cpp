#include <iostream>
#include <cstring>
#define MAXN 2004

using namespace std;

typedef long long ll;
int A[MAXN];
bool used[MAXN];
ll D[MAXN], F[MAXN];
ll M[MAXN][MAXN];
const int MOD = 1000000007LL;

ll DP(int nonp, int pro) {
  if (pro == 0)
    return F[nonp];
  if (nonp == 0)
    return D[pro];
  ll &ans = M[nonp][pro];
  if (ans >= 0)
    return ans;
  return ans = (nonp * DP(nonp-1, pro) + pro * DP(nonp, pro-1)) % MOD;
}

int main() {
  ios::sync_with_stdio(0);
  int n, nonp = 0, pro = 0;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> A[i];
    used[A[i]] = true;
  }
  for (int i = 0; i < n; i++)
    if (A[i] == -1) {
      if (used[i+1])
        nonp ++;
      else
        pro ++;
    }
  D[0] = 1;
  D[1] = 0;
  F[0] = F[1] = 1;
  int last = max(nonp, pro);
  for (int i = 2; i <= last; i++) {
    D[i] = (i-1) * (D[i-1]+D[i-2]);
    D[i] %= MOD;
    F[i] = (i * F[i-1]) % MOD;
  }
  memset(M, -1, sizeof M);
  cout << DP(nonp, pro) << endl;
}

