#include<map>
#include<iostream>
#include<cstdio>
#include<set>
#include<vector>

using namespace std;

typedef pair<string,string> pss;

int main()
{
    int n;
    int d;
    map<pss, vector<int> > M;
    set<pss> ans;
    string s1,s2;
    int ti;
    scanf("%d %d",&n,&d);
    for(int i=0;i<n;i++)
    {
        cin>>s1>>s2;
        scanf("%d",&ti);
        pss t1(s1,s2),t2(s2,s1);
        if(M.find(t2)!=M.end())
        {
            vector<int> &times=M[t2];
            for(int i=times.size()-1,tmp=0;i>=0 and tmp<=d;i--)
            {
                tmp=ti-times[i];
                if(tmp<=d and tmp>0)
                {
                    ans.insert(pss(min(s1,s2),max(s1,s2)));
                    break;
                }
            }
        }
        M[t1].push_back(ti);
    }
    printf("%d\n",ans.size());
    for(auto rel:ans)
        cout<<rel.first<<" "<<rel.second<<"\n";
}
