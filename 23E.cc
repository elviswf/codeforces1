#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 701

using namespace std;

typedef long long ll;
vector<int> adj[MAXN];
ll DP[MAXN][4];
int order[MAXN];
int sz;
int pi[MAXN];
void dfs(int u, int p = -1) {
  pi[u] = p;
  int v;
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i];
    if (v != p)
      dfs(v, u);
  }
  order[sz++] = u;
}

ll T1[MAXN], T2[MAXN], T3[MAXN];

void solve(int u, int ac, int p = -1) {
  ll &ans = DP[u][ac];
  ans = -1;
  int v;
  if (ac >= 2) {
    ans = ac == 2 ? 3 : 1;
    for (int i = 0; i < adj[u].size(); ++i) {
      v = adj[u][i];
      if (v != p)
        ans *= DP[v][0];
    }
    return;
  }
  int nc = 0, sheet = 0;
  ll t1 = 1, t4 = 1;
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i];
    if (v != p) {
      T1[nc] = DP[v][0];
      t1 *= T1[nc];
      T2[nc] = DP[v][ac+1];
      if (ac == 0)
        T3[nc] = DP[v][3];
      if (adj[v].size() == 1)
        ++sheet;
      else
        t4 *= T1[nc];
      nc++;
    }
  }
  ans = (ac+1+sheet) * t4;
  if (ac == 0) {
    for (int i = 0; i < nc; ++i)
      for (int j = i+1; j < nc; ++j)
        ans = max(ans, t1 / (T1[i]*T1[j]) * T3[i]*T3[j] * 3);
  }
  ans = max(ans, (ac+1) * t1);
  for (int i = 0; i < nc; ++i)
    ans = max(ans, t1 / T1[i] * T2[i]);
  return;
}

int main() {
  int n;
  cin >> n;
  int u, v;
  for (int i = 1; i < n; ++i) {
    cin >> u >> v;
    adj[--u].push_back(--v);
    adj[v].push_back(u);
  }
  sz = 0;
  dfs(0);
  for (int i = 0; i < n; ++i)
    for (int j = 0; j <= 3; ++j)
      solve(order[i], j, pi[order[i]]);
  cout << DP[0][0] << endl;
}
