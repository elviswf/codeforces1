#include <iostream>
#include <algorithm>

#define MAXN 101

int A[MAXN], sz;

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, w, x, s[] = {0, 0};
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> w;
    for (int j = 0; j < w; ++j) {
      cin >> x;
      if ((w & 1) == 0)
        s[j >= (w>>1)] += x;
      else if (j != (w>>1))
        s[j > (w>>1)] += x;
      else
        A[sz++] = x;
    }
  }
  sort(A, A+sz);
  reverse(A, A+sz);
  for (int i = 0; i < n; ++i)
    s[i & 1] += A[i];
  cout << s[0] << " " << s[1] << endl;
}
