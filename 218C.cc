#include <iostream>
#include <vector>
#include <bitset>
#define MAXN 101

using namespace std;

int C[MAXN];
inline int findSet(int i) {
  return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
  C[findSet(i)] = findSet(j);
}

vector<int> x[1001];
vector<int> y[1001];

int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    C[i] = i;
  int xx, yy;
  for (int i = 0; i < n; i++) {
    cin >> xx >> yy;
    x[xx].push_back(i);
    y[yy].push_back(i);
  }
  for (int k = 1; k <= 1000; k++)
    for (int i = 1; i < x[k].size(); i++)
      unionSet(x[k][i], x[k][i-1]);
  for (int k = 1; k <= 1000; k++)
    for (int i = 1; i < y[k].size(); i++)
      unionSet(y[k][i], y[k][i-1]);
  bitset<MAXN> B;
  for (int i = 0; i < n; i++)
    B.set(findSet(i));
  cout << (B.count() - 1) << endl;
}

