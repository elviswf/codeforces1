#include <cstdio>
#define MAXN 2005

using namespace std;

char s[MAXN];

int main() {
  int n, q, op, ans = 0;
  scanf("%d\n", &n);
  for (int i = 0; i < n; ++i) {
    gets(s);
    ans += s[i<<1] - '0';
  }
  ans &= 1;
  scanf("%d\n", &q);
  while (q--) {
    gets(s);
    if (s[0] == '3')
      putchar('0' + ans);
    else
      ans = 1 - ans;
  }
  puts("");
}
