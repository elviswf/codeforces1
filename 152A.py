n,m = [int(x) for x in raw_input().split()]
A = [''] * n
suc = set()
for i in range(n):
    A[i] = raw_input()

def findmax(c):
    m = -1
    for i in range(n):
        if m < int(A[i][c]) :
            m = int(A[i][c])
    for i in range(n):
        if m == int(A[i][c]):
            suc.add(i)

for i in range(m):
    findmax(i)
print len(suc)
