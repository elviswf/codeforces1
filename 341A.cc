#include <iostream>
#include <algorithm>
#define MAXN 100004

using namespace std;

typedef long long ll;
ll A[MAXN];

int main() {
  int n;
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  sort(A, A+n);
  ll ans = 0;
  ll ac = A[n-1];
  for (int i = 0; i < n-1; i++)
    ac += A[n-1] - A[i];
  ans += ac;
  for (int i = n - 2; i >= 0; i--) {
    ll d = A[i+1]-A[i];
    ac -= d * (i+2);
    ac += d * (n-i-1);
    ans += ac;
  }
  ll g = __gcd((ll) n, ans);
  ll den = n / g;
  ans /= g;
  cout << ans << " " << den << endl;
}

