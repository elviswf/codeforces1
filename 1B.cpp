#include<cctype>
#include<iostream>
#include<cstdio>
#include<algorithm>

using namespace std;

string numbers(int n)
{
	string r;
	while(n>0)
	{
		r.push_back('0'+n%10);
		n/=10;
	}
	reverse(r.begin(),r.end());
	return r;
}

string letters(int n)
{
	string r;
	while(n>0)
	{
		n--;
		r.push_back('A'+n%26);
		n/=26;
	}
	reverse(r.begin(),r.end());
	return r;
}

bool primer(string s)
{
	if(s[0]!='R')
		return false;
	if(!isdigit(s[1]))
		return false;
	int i;
	for(i=2;i<s.size() and isdigit(s[i]);i++);
	if(i==s.size())
		return false;
	return true;
}

int main()
{
	int n;
	scanf("%d",&n);
	string s;
	while(n--)
	{
		cin>>s;
		if(primer(s))
		{
			for(int i=0;i<s.size();i++)
				if(!isdigit(s[i]))
					s[i]=' ';
			int r,c;
			sscanf(s.c_str(),"%d %d",&r,&c);
			cout<<letters(c)+numbers(r)<<endl;
		}
		else
		{
			int r=0,c=0,i;
			for(i=0;isalpha(s[i]);i++)
				c=c*26+(s[i]-'A'+1);
			for(;i<s.size();i++)
				r=r*10+(s[i]-'0');
			printf("R%dC%d\n",r,c);
		}
	}
}
