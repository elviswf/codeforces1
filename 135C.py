s = input()
n = len(s)
zeros, ones, other = 0, 0, 0
for x in s:
  zeros += x == '0'
  ones += x == '1'
  other += x == '?'

A = ['00', '01', '10', '11']
pos = [0] * 4
pos[0] = zeros + other > n // 2
pos[3] = zeros < n // 2
if zeros <= n // 2 and zeros + other >= n // 2:
  canuse = other - n // 2 + zeros
  pos[1] = s[n-1] == '1' or (s[n-1] == '?' and canuse)
  canuse = zeros < n // 2
  pos[2] = s[n-1] == '0'  or (s[n-1] == '?' and canuse)

for i in range(4):
  if pos[i]:
    print(A[i])
