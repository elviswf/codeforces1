#include <cstdio>
#include <bits/stl_algobase.h>
using std::max;

int main() {
    int n, ans = 1, h, m, h1, m1, t;
    scanf("%d", &n);
    scanf("%d %d", &h, &m);
    n--;
    t = 1;
    while(n--) {
        scanf("%d %d", &h1, &m1);
        if(h1==h and m1==m)
            ans = max(ans, ++t);
        else {
            h = h1;
            m = m1;
            t = 1;
        }
    }
    printf("%d\n", ans);
}
