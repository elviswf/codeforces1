#include <iostream>

using namespace std;

int main() {
  int a, x, y, ans = -1;
  cin >> a >> x >> y;
  if (y > 0 and y % a != 0) {
    int l = y / a;
    if (l == 0) {
      if (x > - a / 2. and x < a / 2.)
        ans = 1;
    } else if (l % 2) {
      if (x > - a / 2. and x < a / 2.)
        ans = 2 + l / 2 * 3;
    } else {
      if (x > -a and x < 0)
        ans = l / 2 * 3;
      else if (x > 0 and x < a)
        ans = l / 2 * 3 + 1;
    }
  }
  cout << ans << endl;
}
