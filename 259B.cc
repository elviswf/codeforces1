#include <iostream>
#define MAXN 100001

using namespace std;

int A[MAXN];

int main() {
  int n, k;
  cin >> n >> k;
  for (int i = 0; i < n; ++i) {
    if (k) {
      A[2*i] = 2*i + 1;
      A[2*i+1] = 2*i + 2;
      --k;
    } else {
      A[2*i] = 2*i + 2;
      A[2*i+1] = 2*i + 1;
    }
  }
  cout << A[0];
  for (int i = 1; i < (n<<1); ++i)
    cout << " " << A[i];
  cout << endl;
}
