#include<vector>
#include<cstdio>
#include<iostream>

using namespace std;
vector<int> P(27, 0);
vector<int> S[100005];

vector<int> operator - (const vector<int> &a,const vector<int> &b) {
  vector<int> c(a.size());
	for (int i = 0; i < a.size(); i++)
    c[i] = a[i] - b[i];
	return c;
}

int val(char c) {
	if (c == '?')
		return 26;
	return c-'a';
}

bool isgood(const vector<int> &A) {
	int diff = 0;
	for (int i = 0; i < 26; i++) {
		if(A[i] > P[i])
			return false;
		diff += P[i] - A[i];
	}
	return diff == A[26];
}

int main() {
	string s, p;
	cin >> s >> p;
	for (int i = 0; i < s.size(); i++)
    S[i].assign(27, 0);
	for (int i = 0; i < 27; i++)
		S[0][i] = 0;
	S[0][val(s[0])] = 1;
	for (int i = 1; i < s.size(); i++) {
    S[i] = S[i-1];
		S[i][val(s[i])]++;
	}
	for (int i = 0; i < p.size(); i++)
		P[val(p[i])]++;
	int ans = 0;
	if(p.size() <= s.size())
		ans = isgood(S[p.size()-1]);
	for(int i = 1; i + p.size() <= (int)s.size(); i++)
		ans += isgood(S[i+p.size()-1]-S[i-1]);
  cout << ans << endl;
}
