#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 300001
#define MOD 1000000007

using namespace std;

int n;
int ti = 0;
int P[MAXN], L[MAXN], H[MAXN];
vector<int> adj[MAXN];
void dfs(int u, int h = 0) {
  P[u] = ti++;
  H[u] = h;
  for (int i = 0; i < adj[u].size(); ++i)
    dfs(adj[u][i], h+1);
  L[u] = ti - 1;
}

typedef long long ll;
#define MAXC 1048580
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1

inline ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

struct ST {
  ll *st, *add;
  ST() {
    st = new ll[MAXC];
    add = new ll[MAXC];
    memset(st, 0, MAXC<<3);
    memset(add, 0, MAXC<<3);
  }
  ~ST() {
    delete[] st;
    delete[] add;
  }

  inline void push(int idx, int l, int r, int L, int R) {
    if (add[idx]) {
      st[idx] = san(st[idx] + add[idx] * (r-l+1)); //update function
      if (l != r) {
        add[L] = san(add[L] + add[idx]);
        add[R] = san(add[R] + add[idx]);
      }
      add[idx] = 0;
    }
  }

  void stupdate(int i, int j, ll up,
      int idx = 0, int l = 0, int r = n - 1) {
    int L = left(idx), R = L + 1;
    if (l >= i and r <= j)
      add[idx] = san(add[idx] + up);
    push(idx, l, r, L, R);
    if ((l >= i and r <= j) or r < i or l > j)
      return;
    int mid = center(l, r);
    stupdate(i, j, up, L, l, mid);
    stupdate(i, j, up, R, mid + 1, r);
    st[idx] = san(st[L] + st[R]);
  }

  ll stq(int i, int idx = 0, int l = 0, int r = n - 1) {
    int L = left(idx), R = L + 1;
    push(idx, l, r, L, R);
    if (l >= i and r <= i)
      return st[idx];
    if (r < i or l > i)
      return 0;
    int mid = center(l, r);
    return san(stq(i, L, l, mid) + stq(i, R, mid+1, r));
  }

};

int main() {
  std::ios::sync_with_stdio(false);
  int m, op, v;
  ll x, k;
  cin >> n;
  for (int i = 1; i < n; ++i) {
    cin >> x;
    adj[x-1].push_back(i);
  }
  dfs(0);
  cin >> m;
  ST A, B;
  ll ans;
  while (m--) {
    cin >> op >> v;
    --v;
    if (op == 2) {
      ans = (A.stq(P[v]) - B.stq(P[v]) * H[v]) % MOD;
      if (ans < 0)
        ans += MOD;
      cout << ans << endl;
    } else {
      cin >> x >> k;
//      cerr << P[v] << " " << L[v] << " " << x + H[v] * k << endl;
      A.stupdate(P[v], L[v], san(x + H[v] * k));
      B.stupdate(P[v], L[v], k);
    }
  }
}
