#include <algorithm>
#include <vector>
#include <cstdio>

using namespace std;

typedef pair<int, int> pii;

int main() {
    int s, n;
    scanf("%d %d", &s, &n);
    vector<pii> A(n);
    for(int i=0; i<n; i++)
        scanf("%d %d", &A[i].first, &A[i].second);
    sort(A.begin(), A.end());
    for(int i=0; i<n; i++) {
        if(s<=A[i].first) {
            puts("NO");
            return 0;
        }
        s += A[i].second;
    }
    puts("YES");
}
