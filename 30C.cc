#include <iostream>
#include <algorithm>
#include <cstdio>
#define MAXN 1001

using namespace std;

typedef long long type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;
pt A[MAXN];
int T[MAXN], id[MAXN], n;
double P[MAXN];

bool order(int i, int j) {
  return T[i] < T[j];
}
bool used[MAXN];
double M[MAXN];

double DP(int i) {
  double &ans = M[i];
  if (used[i])
    return ans;
  used[i] = true;
  ans = 0;
  int ix = id[i], jx;
  for (int j = i+1; j < n; ++j) {
    jx = id[j];
    type dt = T[jx] - T[ix];
    if (dt*dt >= (A[ix]-A[jx]).norm2())
      ans = max(ans, DP(j));
  }
  ans += P[ix];
  return ans;
}

int main() {
  cin >> n;
  for (int i = 0; i < n; ++i) {
    id[i] = i;
    cin >> A[i].x >> A[i].y >> T[i] >> P[i];
  }
  sort(id, id+n, order);
  double ans = 0;
  for(int i = 0; i < n; ++i)
    ans = max(ans, DP(i));
  printf("%.7f\n", ans);
}

