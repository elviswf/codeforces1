#include <iostream>
#include <algorithm>
#define MAXN 100005

using namespace std;

int A[MAXN], D[MAXN], n, m, w, L[MAXN];

bool go(int h) {
  fill(L, L+n, 0);
  for (int i = 0; i < n; ++i)
    D[i] = max(h - A[i], 0);
  int pow = 0, cap = m, d;
  for (int i = 0; i < n; ++i) {
    pow -= L[i];
    //cerr << "> " << pow << endl;
    d = max(D[i] - pow, 0);
    if (d > cap)
      return false;
    pow += d;
    cap -= d;
    if (i + w < n)
      L[i+w] += d;
  }
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  int mini = 2e9;
  cin >> n >> m >> w;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    mini = min(mini, A[i]);
  }
  int lo = mini, hi = mini + m + 1, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    if (go(mid))
      lo = mid;
    else
      hi = mid;
  }
  cout << lo << endl;
}
