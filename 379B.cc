#include <iostream>
#include <cstdio>

using namespace std;

int A[301];

int main() {
  int n;
  cin >> n;
  int sum = 0;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    sum += A[i];
  }
  int id = 0;
  bool dir = 0;
  while (true) {
    if (A[id]) {
      putchar('P');
      --A[id];
      --sum;
      if (sum == 0)
        break;
    }
    if (dir) {
      if (id == 0) {
        dir = 0;
        ++id;
        putchar('R');
      } else {
        --id;
        putchar('L');
      }
    } else {
      if (id == n-1) {
        dir = 1;
        --id;
        putchar('L');
      } else {
        ++id;
        putchar('R');
      }
    }
  }
  puts("");
}
