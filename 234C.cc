#include <iostream>
#include <cstdio>
#include <cstring>
#define MAXN 100001

using namespace std;

int pos[MAXN], neg[MAXN];

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
  int n, x;
  ios::sync_with_stdio(0);
  cin >> n;
  memset(pos, 0, sizeof pos);
  memset(neg, 0, sizeof neg);
  for (int i = 0; i < n; i++) {
    cin >> x;
    if (x > 0)
      neg[i] = 1;
    else if (x < 0)
      pos[i] = 1;
    else
      neg[i] = pos[i] = 1;
  }
  for (int i = n - 2; i >= 0; i--)
    pos[i] += pos[i+1];
  for (int i = 1; i < n; i++)
    neg[i] += neg[i-1];
  int ans = 1 << 30;
  for (int i = 0; i < n - 1; i++)
    ans = min(ans, neg[i] + pos[i+1]);
  cout << ans << endl;
}
