M = [list(),list()]
M[0] = [-1]*10000001
M[1] = [-1]*10000001
MOD=1000000007

def f(loc,n):
    if M[loc][n]>=0:
        return M[loc][n]
    ans=0
    if n==0:
        ans=not loc
    else:
        if loc==0:
            ans=(3*f(1,n-1))%MOD
        else:
            ans=(f(0,n-1)+2*f(1,n-1))%MOD
    M[loc][n]=ans
    return ans

n=int(raw_input())
print f(0,n)
