#include <iostream>
using namespace std;

void show(int x, int y) {
  cout << x << " " << y << endl;
}

int main() {
  int n, m;
  cin >> n >> m;
  if (m == 3) {
    if (n <= 4) {
      show(0, 0);
      show(3, 0);
      show(0, 3);
      if (n == 4)
        show(1, 1);
    }
    else {
      cout << -1 << endl;
    }
  }
  else {
    for (int i = 0; i < m; ++i)
      show(i, 10000000 + i*i);
    for (int i = 0; i < n-m; ++i)
      show(i, -10000000 - i*i);
  }
}
