#include <iostream>
#include <cstdio>
#define MAXN 100010

using namespace std;
typedef long double ld;
typedef long long ll;
float P[MAXN];
ld DP[MAXN];

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; i++)
    scanf("%f", &P[i]);
  P[n] = 0;
  DP[n] = DP[n+1] = 0;
  ld cc = 1;
  ld c1 = cc, cj = cc * n, cj2 = cc * n * n, dp = 0;
  for (ll i = n-1; i >= 0; i--) {
    cc = 1 - P[i];
    c1 = c1 * P[i] + cc;
    cj = cj * P[i] + cc * i;
    cj2 = cj2 * P[i] + cc * i * i;
    dp = dp * P[i] + cc * DP[i+1];
    DP[i] = cj2 - 2 * i * cj + i * i * c1 + dp;
  }
  printf("%.07lf\n", (double)DP[0]);
}
