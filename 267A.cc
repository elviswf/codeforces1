#include <cstdio>
#include <algorithm>
using namespace std;

int f(int a, int b) {
  if (a > b)
    swap(a, b);
  if (a == 0)
    return 0;
  return b/a + f(a, b%a);
}

int main() {
  int n, a, b;
  scanf("%d", &n);
  while (n--) {
    scanf("%d %d", &a, &b);
    printf("%d\n", f(a, b));
  }
}
