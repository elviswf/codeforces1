M=[1]*1001
P=[-1]*1001

def pow2(n):
	if(P[n]!=-1):
		return P[n]
	if(n==0):
		return 1
	tmp=pow2(n/2)
	tmp=(tmp*tmp)%1000003
	if(n%2==1):
		tmp=(tmp*2)%1000003
	P[n]=tmp
	return tmp

def solve(n):
	for i in range(1,n+1):
		rpta=0
		for j in range(i):
			rpta=(rpta+(pow2(j)*M[i-j-1])%1000003)%1000003
		M[i]=rpta
	return M[n]


n=int(raw_input())
print solve(n)
