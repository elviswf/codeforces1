#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef pair<int, int> pii;

int main() {
  vector<int> X, Y;
  vector<pii> S;
  int x, y;
  for (int i = 0; i < 8; ++i) {
    cin >> x >> y;
    S.push_back(pii(x, y));
    X.push_back(x);
    Y.push_back(y);
  }
  sort(X.begin(), X.end());
  X.resize(unique(X.begin(), X.end()) - X.begin());
  sort(Y.begin(), Y.end());
  Y.resize(unique(Y.begin(), Y.end()) - Y.begin());
  sort(S.begin(), S.end());
  int k = 0;
  bool valid = X.size() == 3 and Y.size() == 3;
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j) {
      if (i != 1 or i != j) {
        valid &= S[k].first == X[i] and S[k].second == Y[j];
        ++k;
      }
    }
  cout << (valid ? "respectable" : "ugly") << endl;
}
