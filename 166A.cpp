#include<algorithm>
#include<vector>
#include<cstdio>

using namespace std;

typedef pair<int,int> pii;

bool order(pii a,pii b)
{
    if(a.first==b.first)
        return a.second<b.second;
    return a.first>b.first;
}

int main() {
    int n,k;
    scanf("%d %d",&n,&k);
    k--;
    vector<pii> A(n);
    for(int i=0;i<n;i++)
        scanf("%d %d",&A[i].first,&A[i].second);
    sort(A.begin(),A.end(),order);
    int c1,c2;
    for(c1=0;k-c1-1>=0 and A[k-c1-1]==A[k];c1++);
    for(c2=0;k+c2+1<n and A[k+c2+1]==A[k];c2++);
    printf("%d\n",c1+c2+1);
}
