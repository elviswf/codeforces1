from math import sqrt,ceil

def isprime(n):
	i=2
	while(i*i<=n):
		if(n%i==0):
			return 0
		i+=1
	return 1

s=raw_input().split()
n=int(s[0])
m=int(s[1])

for i in range(n+1,m):
	if(isprime(i)):
		print 'NO'
		exit(0)
if(isprime(m)):
	print 'YES'
else:
	print 'NO'
