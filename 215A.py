n = int(raw_input())
A = [int(x) for x in raw_input().split()]
m = int(raw_input())
B = [int(x) for x in raw_input().split()]
maxi = 0
for a in A:
    for b in B:
        if b % a == 0:
            maxi = max(maxi, b/a)
ans = 0
for a in A:
    for b in B:
        if b % a == 0:
            ans += maxi == b/a
print ans
