#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  int n;
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
  scanf("%d", &n);
  vector<int> A(n);
  for (int i = 0; i < n; i++)
    scanf("%d", &A[i]);
  sort(A.begin(), A.end());
  int ans = 1;
  vector<int>::iterator it, i;
  for (i = A.begin()+1; i != A.end(); i++) {
    it = lower_bound(A.begin(), i+1, (*i+1)/2);
    ans = max(ans, (int)(i-it)+1);
  }
  printf("%d\n", n-ans);
}
