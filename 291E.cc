#include <iostream>
#include <vector>
#define MAXN 100005

using namespace std;

vector<int> adj[MAXN];
string P;
string go[MAXN];
int N, M;
int pi[3*MAXN];

void kmpPreprocess() {
  int i = 0, j = -1;
  M = P.size();
  pi[0] = -1;
  while(i < M) {
    while(j>=0 and P[i] != P[j])
      j = pi[j];
    i ++, j++;
    pi[i] = j;
  }
}

int dfs(int u, int j = 0) {
  int ans = 0;
  string &T = go[u];
  int i = 0, n = T.size();
  while (i < n) {
    while (j >= 0 and T[i] != P[j])
      j = pi[j];
    i++, j++;
    if (j == M) {
      ans ++;
      j = pi[j];
    }
  }
  for (int i = 0; i < adj[u].size(); i++)
    ans += dfs(adj[u][i], j);
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> N;
  int x;
  for (int i = 1; i < N; i++) {
    cin >> x;
    x --;
    cin >> go[i];
    adj[x].push_back(i);
  }
  cin >> P;
  kmpPreprocess();
  cout << dfs(0) << endl;
}
