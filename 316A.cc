#include <iostream>
#include <cstring>

using namespace std;

char there[10];

int main() {
  ios::sync_with_stdio(0);
  string s;
  cin >> s;
  memset(there, 1, sizeof there);
  long long ans = 1;
  int ten = 0;
  for (int i = 0; i < s.size(); i++) {
    if (s[i] == '?') {
      if (i)
        ten ++;
      else
        ans *= 9;
    } else if (s[i] >= 'A' and s[i] <= 'J' and there[s[i] - 'A'] == 1) {
      there[s[i]- 'A'] = i ? 10 : 9;
    }
  }
  bool first = false;
  int can = 0;
  for (int i = 0; i < 10; i++) {
    if (there[i] == 9)
      first = true;
    else if (there[i] == 10)
      can ++;
  }
  if (first) {
    for (int i = 0; i < can; i++)
      ans *= 9 - i;
    ans *= 9;
  } else {
    for (int i = 0; i < can; i++)
      ans *= 10 - i;
  }
  cout << ans;
  while (ten --)
    cout << 0;
  cout << endl;
}
