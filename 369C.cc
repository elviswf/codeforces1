#include <iostream>
#include <vector>
#define MAXN 100001

using namespace std;

typedef pair<int, bool> pib;

vector<pib> adj[MAXN];
vector<int> ans;
bool h[MAXN];

void pre(int u, int p = -1) {
  int v;
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i].first;
    if (v != p) {
      h[v] = adj[u][i].second;
      pre(v, u);
      h[u] |= h[v];
    }
  }
}

void dfs(int u, int p = -1) {
  int v;
  int cnt = 0;
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i].first;
    if (v != p) {
      dfs(v, u);
      cnt += h[adj[u][i].first];
    }
  }
  //cout << u+1 << ": " << cnt << " " << h[u] << endl;
  if (not cnt and h[u])
    ans.push_back(u);
}

int main() {
  ios::sync_with_stdio(0);
  int n, u, v, x;
  cin >> n;
  for (int i = 1; i < n; ++i) {
    cin >> u >> v >> x;
    --u, --v, --x;
    adj[u].push_back(pib(v, x));
    adj[v].push_back(pib(u, x));
  }
  pre(0);
  dfs(0);
  cout << ans.size() << endl;
  if (ans.size()) {
    cout << ans[0]+1;
    for (int i = 1; i < ans.size(); ++i)
      cout << " " << ans[i]+1;
  }
  cout << endl;
}
