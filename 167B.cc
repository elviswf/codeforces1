#include <iostream>
#include <cstdio>
#define MAXN 205

using namespace std;

double A[2*MAXN][MAXN], B[2*MAXN][MAXN];
int p[MAXN], a[MAXN];

int main() {
  int n, l, k;
  cin >> n >> l >> k;
  k = min(k, n);
  for (int i = 0; i < n; i++)
    cin >> p[i];
  for (int i = 0; i < n; i++)
    cin >> a[i];
  for (int c = -n; c <= n; c++)
    for (int w = 0; w <= n; w++)
      B[c+n][w] = w >= l and c >= 0;
  for (int id = n - 1; id >= 0; id--) {
    for (int c = -id; c <= n; c++)
      for (int w = 0; w <= id; w++) {
        if (a[id] >= 0)
          A[c+n][w] = B[min(c+a[id], n)+n][w+1];
        else
          A[c+n][w] = B[c-1+n][w+1];
        A[c+n][w] *= p[id] / 100.;
        A[c+n][w] += (100-p[id])/100. * B[c+n][w];
      }
    for (int c = -id; c <= n; c++)
      for (int w = 0; w <= id; w++)
        B[c+n][w] = A[c+n][w];
  }
  printf("%.8f\n", A[k+n][0]);
}
