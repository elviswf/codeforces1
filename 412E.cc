#include <iostream>

using namespace std;

string s;

inline bool letter(char c) {
  return c >= 'a' and c <= 'z';
}
inline bool digit(char c) {
  return c >= '0' and c <= '9';
}

inline bool valid1(char c) {
  return letter(c) or digit(c) or c == '_';
}

inline bool valid2(char c) {
  return letter(c) or digit(c);
}

inline bool valid3(char c) {
  return letter(c);
}

typedef long long ll;

int main(void) {
  cin >> s;
  int n = s.size();
  int arr = 0, dot;
  ll ans = 0;
  while (arr < n) {
    if (s[arr] == '@') {
      for (dot = arr+1; dot < n and valid2(s[dot]); ++dot) { }
      if (dot > arr + 1 and dot < n and s[dot] == '.') {
        int i, j, c = 0;
        for (i = arr-1; i >= 0 and valid1(s[i]); --i)
          c += letter(s[i]);
        for (j = dot+1; j < n and valid3(s[j]); ++j) { }
        //cerr << c << " " << j-dot-1 << endl;
        ans += c * (j - dot - 1LL);
        arr = j;
      } else arr = dot;
    } else ++arr;
  }
  cout << ans << endl;
  return 0;
}
