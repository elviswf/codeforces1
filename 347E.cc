#include <iostream>
#include <algorithm>

using namespace std;
#define MAXN 1000004

int X[MAXN];
int main() {
  ios::sync_with_stdio(0);
  int n, a, b;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> X[i];
  sort(X, X+n);
  cin >> a >> b;
  int ans = 0, mod;
  /*
  while (a > b and n) {
    mod = a % X[n-1];
    cout << a << " " << mod << ": " << ans << endl;
    if (mod and a - mod >= b) {
      a -= mod;
      ans ++;
    } else {
      ans += (a-b) / X[n-1] * 2;
      a -= (a-b) / X[n-1] * X[n-1];
      n--;
    }
  }
  ans += a - b;
  */
  while (a > b and n) {
    int desp = 1;
    for (int i = 0; i < n; i++)
      desp = max(desp, a % X[i]);
    ans ++;
    cout << "> " << a << endl;
    a -= desp;
  }
  cout << ans << endl;
}
