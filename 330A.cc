#include <iostream>
#include <cstring>

using namespace std;

bool cell[11][11];
bool vc[11];
int main() {
  int r, c;
  cin >> r >> c;
  memset(cell, 1, sizeof cell);
  memset(vc, 1, sizeof vc);
  string s;
  int ans = 0;
  for (int i = 0; i < r; i++) {
    bool vr = true;
    cin >> s;
    for (int j = 0; j < c; j++)
      if (s[j] == 'S') {
        vr = false;
        vc[j] = false;
      }
    if (vr)
      for (int j = 0; j < c; j++) {
        ans += cell[i][j];
        cell[i][j] = 0;
      }
  }
  for (int j = 0; j < c; j++)
    if (vc[j])
      for (int i = 0; i < r; i++)
        ans += cell[i][j];
  cout << ans << endl;
}
