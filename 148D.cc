#include <iostream>
#include <cstdio>

using namespace std;

double M[1010][1010][3];

double DP(int w, int b, int jog) {
  double &ans = M[w][b][jog];
  if (ans >= 0)
    return ans;
  if (w == 0)
    return 0;
  if (jog == 0) {
    return ans = (w+(b?b*DP(w, b-1, 1):0))/(w+b);
  } else {
    if (not b)
      return 0;
    return ans = b
        *(w*DP(w-1,b-1,0)+(b-1?(b-1)*DP(w,b-2,0):0))/(w+b-1)/(w+b);
  }
}

int main() {
  int w, b;
  cin >> w >> b;
  for (int i = 0; i <= w; i++)
    for (int j = 0; j <= b; j++)
      for (int k = 0; k <= 2; k++)
        M[i][j][k] = -1;
  printf("%.09lf\n", DP(w, b, 0));
}
