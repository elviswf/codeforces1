#include <vector>
#include <map>
#include <cstring>
#include <cstdio>
#define MOD 1000000007

using namespace std;
typedef long long ll;

inline ll san(ll x) {
  return x >= MOD ? x % MOD : x;
}

ll powmod(ll a, ll e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = san(a * r);
    a = san(a * a);
    e >>= 1;
  }
  return r;
}

bool lucky(ll x) {
  int t;
  while (x) {
    t = x % 10;
    if (t != 4 and t != 7)
      return false;
    x /= 10;
  }
  return true;
}

#define inv(x) powmod(x, MOD-2)

ll C[100001];

void comb(int n) {
  ll ans = 1;
  for (int i = 0; i <= n; ++i) {
    C[i] = ans;
    ans = san(ans * (n-i));
    ans = san(ans * inv(i+1));
  }
}

ll M[1024][1024];
int n, k;
vector<int> A;

ll DP(int id, int ac) {
  ll &ans = M[id][ac];
  if (ans >= 0)
    return ans;
  if (ac == k)
    return ans = 1;
  if (id == 0)
    return ans = ac + A[id] >= k ? C[k - ac] : 0;
  ans = DP(id-1, ac) + A[id] * DP(id-1, ac+1);
  if (ans >= MOD)
    ans %= MOD;
  return ans;
}

int main() {
  scanf("%d %d", &n, &k);
  int x;
  map<ll, int> T;
  T[0] = 0;
  for (int i = 0; i < n; ++i) {
    scanf("%d", &x);
    T[lucky(x) ? x : 0] ++;
  }
  A.reserve(T.size());
  for (map<ll, int>::iterator it = T.begin(); it != T.end(); ++it)
    A.push_back(it->second);
  memset(M, -1, sizeof M);
  comb(A[0]);
  printf("%d\n", (int)DP(A.size()-1, 0));
}
