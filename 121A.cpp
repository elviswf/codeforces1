#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

int digits(int n)
{
	int r=0;
	while(n)
	{
		r++;
		n/=10;
	}
	return r;
}

int main()
{
	int l,r,sup;
	long long tmp;
	scanf("%d %d",&l,&r);
	vector<long long> luck;
	sup=digits(r);
	for(int i=digits(l);i<=sup+1;i++)
	{
		for(long long j=0;j<(1LL<<i);j++)
		{
			tmp=0;
			for(int k=0;k<i;k++)
				tmp=tmp*10+(j&(1LL<<k)?4:7);
			if(tmp>=l)
				luck.push_back(tmp);
		}
	}
	sort(luck.begin(),luck.end());
	long long ans=0;
	int i,ant=l-1;
	for(i=0;luck[i]<r;i++)
	{
	//	printf("#%lli\n",luck[i]);
		ans+=(luck[i]-ant)*luck[i];
		ant=luck[i];
	}
	ans+=(r-ant)*luck[i];
	printf("%I64d\n",ans);
}
