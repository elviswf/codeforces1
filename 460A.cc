#include <iostream>

using namespace std;

int main() {
  int n, m, i;
  cin >> n >> m;
  for (i = 1; n; ++i) {
    --n;
    if (i % m == 0)
      ++n;
  }
  cout << i-1 << endl;
}
