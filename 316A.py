s = input()
there = [False for i in range(10)]
ans = 1
letters = 0
first = False

for i in range(len(s)):
  if s[i] == '?':
    ans *= 10 if i else 9
  elif s[i] >= 'A' and s[i] <= 'J' and not there[ord(s[i])-65]:
    if i:
      letters += 1
    else:
      first = True
    there[ord(s[i])-65] = True

if first:
  for i in range(letters):
    ans *= 9 - i
  ans *= 9

else:
  for i in range(letters):
    ans *= 10 - i

print(ans)
