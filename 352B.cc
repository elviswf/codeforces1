#include <iostream>
#include <cstring>
#define MAXN 100001

using namespace std;

int P[MAXN];
int D[MAXN];
int Q[MAXN];

int main() {
  memset(P, -1, sizeof P);
  int n, x;
  cin >> n;
  int ans = 0;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    if (P[x] == -1 and Q[x] >= 0) {
      P[x] = i;
      Q[x] = 1;
      ans ++;
    } else if (D[x] == 0) {
      D[x] = i - P[x];
      P[x] = i;
      Q[x] ++;
    } else if (i == P[x] + D[x]) {
      P[x] = i;
    } else {
      if (Q[x] != -1)
        ans --;
      Q[x] = -1;
    }
  }
  cout << ans << endl;
  for (int i = 1; i < MAXN; ++i)
    if (Q[i] > 0)
      cout << i << " " << D[i] << endl;
}
