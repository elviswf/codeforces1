#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, sa = 0, sb = 0, a, b;
  cin >> n;
  string ans;
  ans.reserve(n);
  while (n--) {
    cin >> a >> b;
    if (abs(sa+a-sb) <= abs(sa-(sb+b))) {
      sa += a;
      ans.push_back('A');
    } else {
      sb += b;
      ans.push_back('G');
    }
  }
  if (abs(sa-sb) <= 500)
    cout << ans << endl;
  else
    cout << -1 << endl;
}
