import java.util.Map;
import java.util.TreeMap;

public class Cf459D {
    FastScanner sc;
    int n, mv;
    int[] A, L, C;
    Map<Integer, Integer> M;
    BIT B;

    public static void main(String[] args) {
        Cf459D cf = new Cf459D();
        System.out.println(cf.run());
    }

    private int getValue(int i) {
        if (M.containsKey(i))
            return M.get(i);
        M.put(i, mv);
        return mv++;
    }

    Cf459D() {
        sc = new FastScanner();
        mv = 0;
        n = sc.nextInt();
        A = new int[n];
        L = new int[n];
        C = new int[n];
        M = new TreeMap<Integer, Integer>();
        B = new BIT(n);
        for (int i = 0; i < n; ++i)
            A[i] = getValue(sc.nextInt());
    }

    public long run() {
        long ans = 0;
        for (int i = 0; i < n; ++i)
            L[i] = ++C[A[i]];
        C = new int[n];
        for (int i = n - 1; i >= 0; --i) {
            ans += B.query(L[i]-1);
            B.update(++C[A[i]]);
        }
        return ans;
    }

    public class BIT {
        int[] A;
        BIT(int n) {
            A = new int[n+1];
        }
        public int query(int idx) {
            int ans = 0;
            while (idx > 0) {
                ans += A[idx];
                idx -= idx & -idx;
            }
            return ans;
        }
        public void update(int idx) {
            update(idx, 1);
        }
        public void update(int idx, int val) {
            while (idx <= n) {
                A[idx] += val;
                idx += idx & -idx;
            }
        }
    }
}
