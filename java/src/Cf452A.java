import java.util.Scanner;

public class Cf452A {
    public static String[] evee =
            {"vaporeon", "jolteon", "flareon", "espeon", "umbreon", "leafeon", "glaceon", "sylveon"};
    String p;
    int n;
    public static void main(String[] args) {
        Cf452A cf = new Cf452A();
        System.out.println(cf.run());
    }
    public Cf452A() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        p = sc.next();
    }
    public String run() {
        for (int i = 0; i < evee.length; ++i)
            if (validate(evee[i]))
                return evee[i];
        return "";
    }
    boolean validate(String s) {
        if (s.length() != n)
            return false;
        for (int i = 0; i < n; ++i)
            if (p.charAt(i) != '.' && p.charAt(i) != s.charAt(i))
                return false;
        return true;
    }
}
