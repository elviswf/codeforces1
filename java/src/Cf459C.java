/**
 * Created by alculquicondor on 16/08/14.
 */

public class Cf459C {

    private int n, k, d;
    private int [][]ans;
    private FastScanner scanner;

    public static void main(String args[]) {
        Cf459C cf = new Cf459C();
        if (cf.run())
            cf.print();
        else
            System.out.println("-1");
    }

    Cf459C() {
        scanner = new FastScanner();
        n = scanner.nextInt();
        k = scanner.nextInt();
        d = scanner.nextInt();
    }

    public boolean run() {
        if (Math.pow(k, d) < n)
            return false;
        ans = new int[d][n];
        for (int i = 0; i < n; ++i) {
            int s = i;
            for (int j = 0; j < d; ++j) {
                ans[j][i] = (s % k) + 1;
                s /= k;
            }
        }
        return true;
    }

    public void print() {
        for (int i = 0; i < d; ++i) {
            StringBuilder builder = new StringBuilder();
            builder.append(ans[i][0]);
            for (int j = 1; j < n; ++j) {
                builder.append(' ');
                builder.append(ans[i][j]);
            }
            System.out.println(builder.toString());
        }
    }
}
