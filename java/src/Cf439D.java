import java.util.Scanner;

public class Cf439D {
    int n, m;
    int[] A, B;
    public static void main(String[] args) {
        Cf439D cf = new Cf439D();
        System.out.println(cf.run());
    }
    public Cf439D() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        m = sc.nextInt();
        A = new int[n];
        B = new int[m];
        for (int i = 0; i < n; ++i)
            A[i] = sc.nextInt();
        for (int i = 0; i < m; ++i)
            B[i] = sc.nextInt();
    }
    public long run() {
        long lo = 1000000000, hi = 0;
        for (int i = 0; i < n; ++i)
            lo = Math.min(lo, A[i]);
        for (int i = 0; i < m; ++i)
            hi = Math.max(hi, B[i]);
        if (lo > hi)
            return 0;
        long m1, m2;
        while (hi - lo > 2) {
            m1 = (2 * lo + hi) / 3;
            m2 = (lo + 2 * hi) / 3;
            if (calc(m1) < calc(m2))
                hi = m2;
            else
                lo = m1;
        }
        long ans = 1L << 60;
        for (long i = lo; i <= hi; ++i)
            ans = Math.min(ans, calc(i));
        return ans;
    }
    private long calc(long x) {
        long ans = 0;
        for (int i = 0; i < n; ++i)
            ans += Math.max(x - A[i], 0);
        for (int i = 0; i < m; ++i)
            ans += Math.max(B[i] - x, 0);
        return ans;
    }
}
