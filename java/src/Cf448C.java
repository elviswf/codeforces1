import java.util.Scanner;

public class Cf448C {
    int n;
    int[] A;
    public static void main(String[] args) {
        Cf448C cf = new Cf448C();
        System.out.println(cf.run());
    }
    public Cf448C() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        A = new int[n];
        for (int i = 0; i < n; ++i)
            A[i] = sc.nextInt();
    }
    public long run() {
        return solve(0, n, 0);
    }
    long solve(int ini, int fin, int h) {
        int mini = A[ini];
        for (int i = ini+1; i < fin; ++i)
            mini = Math.min(mini, A[i]);
        int s = -1;
        long ans = mini - h;
        for (int i = ini; i < fin; ++i) {
            if (A[i] > mini) {
                if (s == -1)
                    s = i;
            } else {
                if (s != -1)
                    ans += solve(s, i, mini);
                s = -1;
            }
        }
        if (s != -1)
            ans += solve(s, fin, mini);
        return Math.min(ans, fin - ini);
    }
}
