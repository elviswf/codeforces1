#include <vector>
#include <iostream>
#include <cstring>
#define MAXN 100005

using namespace std;

int F1[MAXN], F2[MAXN], D1[MAXN], D2[MAXN];
int F3[MAXN], D3[MAXN];
bool isP[MAXN];
vector<int> adj[MAXN];

void dfs1(int u, int p = -1) {
  int v;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (v != p) {
      dfs1(v, u);
      if (F1[v] != -1) {
        if (D1[v] + 1 > D1[u]) {
          F2[u] = F1[u], D2[u] = D1[u];
          F1[u] = F1[v], D1[u] = D1[v] + 1;
        }
        else if (D1[v] + 1 > D2[u]) {
          F2[u] = F1[v], D2[u] = D1[v] + 1;
        }
      }
    }
  }
  if (isP[u] and F1[u] == -1)
    F1[u] = u, D1[u] = 0;
}

void dfs2(int u, int p = -1) {
  int v;
  if (isP[u])
    F3[u] = u, D3[u] = 0;
  if (p != -1) {
    if (F3[p] != -1)
      F3[u] = F3[p], D3[u] = 1 + D3[p];
    if (F1[p] != -1) {
      if (F1[p] == F1[u]) {
        if (F2[p] != -1 and D2[p] + 1 > D3[u])
          F3[u] = F2[p], D3[u] = D2[p] + 1;
      } else if (D1[p] + 1 > D3[u]) {
        F3[u] = F1[p], D3[u] = D1[p] + 1;
      }
    }
  }
  //cout << "$ " << u << " " << F3[u] << " " << D3[u] << endl;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (v != p)
      dfs2(v, u);
  }
}

int main() {
  int n, m, d;
  ios::sync_with_stdio(0);
  memset(F1, -1, sizeof F1);
  memset(F2, -1, sizeof F2);
  memset(F3, -1, sizeof F3);
  memset(D1, -1, sizeof D1);
  memset(D2, -1, sizeof D2);
  memset(D3, -1, sizeof D3);
  cin >> n >> m >> d;
  int u, v;
  memset(isP, 0, sizeof isP);
  for (int i = 0; i < m; i++) {
    cin >> u;
    isP[--u] = true;
  }
  for (int i = 0; i < n-1; i++) {
    cin >> u >> v;
    adj[--u].push_back(--v);
    adj[v].push_back(u);
  }
  dfs1(n-1);
  dfs2(n-1);
  int ans = 0;
  for (int i = 0; i < n; i++) {
    /*
    cout << "> " << F1[i] << "," << D1[i] << " " <<
      F2[i] << "," << D2[i] << " " <<
      F3[i] << "," << D3[i] << endl;
    */
    ans += max(D1[i], D3[i]) <= d;
  }
  cout << ans << endl;
}
