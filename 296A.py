n = int(input())
C = [0] * 1001
for x in input().split():
  C[int(x)] += 1
for x in C:
  if x > (n+1)/2:
    print("NO")
    exit(0)
print("YES")
