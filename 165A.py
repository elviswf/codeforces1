def right(a, b):
    return a[0] > b[0] and a[1] == b[1]
def left(a, b):
    return a[0] < b[0] and a[1] == b[1]
def low(a, b):
    return a[0] == b[0] and a[1] < b[1]
def upper(a, b):
    return a[0] == b[0] and a[1] > b[1]
A = list()
def has(a, fun):
    global A
    for x in A:
        if fun(x, a):
            return True
    return False
n = int(raw_input())
for i in range(n):
    A.append([int(x) for x in raw_input().split()])
a = 0
for x in A:
    a += has(x, right) and has(x, left) and has(x, low) and has(x, upper)
print a
