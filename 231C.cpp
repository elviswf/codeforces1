#include<cstdio>
#include<algorithm>
#include<cstring>
#define MAXN 100001
#define left(i) (i<<1)+1
#define center(l, r) (l+r)>>1

using namespace std;

typedef long long ll;
int n, k;
int A[MAXN]; 
ll tree[4*MAXN], add[4*MAXN];

void update(int i, int j, int up, int idx=0, int l=0, int r=n-1) {
    int L = left(idx), R = L+1, mid = center(l, r);
    tree[idx] += (ll)(r-l+1)*add[idx];
    if(l!=r and add[idx]) {
        add[L] += add[idx];
        add[R] += add[idx];
    }
    add[idx] = 0;
    if(l>=i and  r<=j) {
        tree[idx] += (ll)(r-l+1)*up;
        if(l!=r) {
            add[L] += up;
            add[R] += up;
        }
        return;
    }
    if(l>j or r<i)
        return;
    update(i, j, up, L, l, mid);
    update(i, j, up, R, mid+1, r);
    tree[idx] = tree[L] + tree[R];
}

ll query(int i, int j, int idx=0, int l=0, int r=n-1) {
    int L = left(idx), R = L+1, mid = center(l, r);
    if(add[idx]) {
        tree[idx] += (ll)(r-l+1)*add[idx];
        add[L] += add[idx];
        add[R] += add[idx];
        add[idx] = 0;
    }
    if(l>=i and r<=j)
        return tree[idx];
    if(l>j or r<i)
        return 0;
    return query(i, j, L, l, mid) + query(i, j, R, mid+1, r);
}

int main() {
    scanf("%d %d", &n, &k);
    for(int i=0; i<n; i++)
        scanf("%d", &A[i]);
    sort(A, A+n);
    memset(tree, 0, sizeof tree);
    memset(add, 0, sizeof add);
    int maxi = 1, ans = A[0];
    for(int i=1; i<n; i++) {
        int diff = A[i]-A[i-1];
        if(diff)
            update(0, i-1, diff);
        int lo = 0, hi = i, mid;
        while(lo<hi) {
            mid = center(lo, hi);
            //printf(">%d: %d: %d\n", i, mid, query(mid, i));
            if(query(mid, i)<=k)
                hi = mid;
            else
                lo = mid+1;
        }
        mid = center(lo, hi);
        if((i-mid+1)>maxi) {
            maxi = i-mid+1;
            ans = A[i];
        }
    }
    printf("%d %d\n", maxi, ans);
}
