#include <iostream>
#include <cstring>

using namespace std;

int n;

int A[23];
int pos[23][23];

char M[1<<22][23];

char dp(int mask, int k) {
  char &ans = M[mask][k];
  if (ans >= 0)
    return ans;
  bool poss = false;
  for (int i = 0; not poss and i < k; ++i)
    if (mask & (1<<i))
      poss |= pos[k][i] >= 0 and (mask & (1<<pos[k][i]));
  if (not poss)
    return ans = 100;
  if (k == n-1)
    return 0;
  ans = 1 + dp(mask | (1<<k), k+1);
  for (int i = 0; i < k; ++i)
    if (mask & (1<<i))
      ans = min(ans, dp((mask ^ (1<<i)) | (1<<k), k+1));
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n;
  memset(pos, -1, sizeof pos);
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    for (int j = 0; j < i; ++j)
      for (int k = 0; k < i; ++k)
        if (A[j] + A[k] == A[i])
          pos[i][j] = k;
  }
  memset(M, -1, sizeof M);
  int ans = n == 1 ? 0 : dp(1, 1);
  cout << (ans < 100 ? 1+ans : -1) << endl;
}

