#include <iostream>

using namespace std;

typedef long long ll;
ll n, m, k;

ll go(ll x) {
  return n / (x+1) * (m / (k-x+1));
}

void print() {
  for (int i = 0; i <= k; ++i)
    cerr << ' ' << i << ' ' << go(i) << endl;
}

int main() {
  cin >> n >> m >> k;
  //print();
  int lo = max(k-m+1, 0LL), hi = min(k, n-1), m1, m2;
  ll ans = 0;
  if (lo <= hi)
    ans = max(go(lo), go(hi));
  cout << (ans ? ans : -1) << endl;
}
