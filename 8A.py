def go(s,a,b):
	index=-1
	for i in range(len(s)):
		j=0
		while j<len(a) and i+j<len(s) and s[i+j]==a[j]:
			j+=1
		if j==len(a):
			index=i+j
			break
	if index==-1:
		return 0
	dd=index
	index=-1
	for i in range(dd,len(s)):
		j=0
		while j<len(b) and i+j<len(s) and s[i+j]==b[j]:
			j+=1
		if j==len(b):
			return 1
	return 0
	
s=raw_input()
a=raw_input()
b=raw_input()
forw=go(s,a,b)
back=go(s[::-1],a,b)
if forw:
	if back:
		print 'both'
	else:
		print 'forward'
else:
	if back:
		print 'backward'
	else:
		print 'fantasy'
