#include <iostream>
#define MAXN 100005

using namespace std;

int A[MAXN], L[MAXN], R[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, x;
  cin >> n;
  A[0] = -1;
  cin >> A[1];
  for (int i = 2; i <= n; ++i)
    cin >> A[i];
  A[n+1] = 1000000005;
  for (int i = 1; i <= n; ++i)
    L[i] = A[i] > A[i-1] ? L[i-1] + 1 : 1;
  for (int i = n; i; --i)
    R[i] = A[i] < A[i+1] ? R[i+1] + 1 : 1;
  int ans = 0;
  for (int i = 1; i <= n; ++i) {
    if (A[i+1] > A[i-1] + 1)
      ans = max(ans, L[i-1] + R[i+1] + 1);
    ans = max(ans, 1 + L[i-1]);
    ans = max(ans, 1 + R[i+1]);
  }
  cout << ans << endl;
}
