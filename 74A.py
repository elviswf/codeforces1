def points(w,l,v):
	r=100*w-l*50
	for x in v:
		r+=x
	return r

n=int(raw_input())
v=[0]*n
for i in range(n):
	s=raw_input().split()
	v[i]=(s[0],points(int(s[1]),int(s[2]),[int(x) for x in s[3:]]))
print max(v,key=lambda a:a[1])[0]
