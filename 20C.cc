#include <cstdio>
#include <cstring>
#include <queue>
#define MAXN 100005

using namespace std;
int n;
typedef pair<int, int> pii;
typedef long long ll;
vector<pii> adj[MAXN];
bool visit[MAXN];
ll d[MAXN];
int pi[MAXN];

int main() {
  int m, u, v, w;
  scanf("%d %d", &n, &m);
  for (int i = 1; i <= n; i++)
    d[i] = 1LL << 60;
  while (m--) {
    scanf("%d %d %d", &u, &v, &w);
    adj[u].push_back(pii(v, w));
    adj[v].push_back(pii(u, w));
  }
  memset(visit, 0, sizeof visit);
  priority_queue<pii, vector<pii>, greater<pii> > Q;
  Q.push(pii(0, 1));
  d[1] = 0;
  while (not Q.empty()) {
    u = Q.top().second;
    Q.pop();
    if (visit[u])
      continue;
    visit[u] = true;
    for (int i = 0; i < adj[u].size(); i++) {
      v = adj[u][i].first;
      w = adj[u][i].second;
      if (d[v] > d[u] + w) {
        d[v] = d[u] + w;
        Q.push(pii(d[v], v));
        pi[v] = u;
      }
    }
  }
  if (d[n] < (1LL<<60)) {
    vector<int> ans;
    v = n;
    while (v != 1) {
      ans.push_back(v);
      v = pi[v];
    }
    printf("1");
    for (int i = ans.size() - 1; i >= 0; i--)
      printf(" %d", ans[i]);
    puts("");
  } else {
    puts("-1");
  }
}
