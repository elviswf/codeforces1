#include <iostream>
#include <vector>
#include <sstream>
#define MAXN 100001

using namespace std;

typedef long long ll;
bool isC[MAXN];
int D[MAXN];

void pre(int n) {
  vector<int> P;
  for (int i = 2; i <= n; ++i)
    if (not isC[i]) {
      P.push_back(i);
      for (ll j = (ll)i * i; j <= n; j += i)
        isC[j] = true;
    }
  for (int i = 4; i <= n; i += 2) {
    for (auto p: P)
      if (not isC[i - p]) {
        D[i] = p;
        break;
      }
  }
}

int A[MAXN], P[MAXN], moves;
stringstream ans;

void change(int i, int j) {
  ++moves;
  ans << i+1 << " " << j+1 << endl;
  swap(A[i], A[j]);
  swap(P[A[i]], P[A[j]]);
}

int main() {
  std::ios::sync_with_stdio(false);
  int n;
  cin >> n;
  pre(n);
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    --A[i];
    P[A[i]] = i;
  }
  int d;
  for (int i = 0; i < n; ++i) {
    d = P[i] - i + 1;
    if (not (d & 1)) {
      change(P[i]-1, P[i]);
      --d;
    }
    if (d > 1) {
      if (isC[d]) {
        change(i + D[d+1] - 1, P[i]);
        change(i, i + D[d+1] - 1);
      } else {
        change(i, P[i]);
      }
    }
  }
  cout << moves << endl;
  cout << ans.str();
}
