n,m=[int(x) for x in raw_input().split()]
A=[]
for i in range(m):
	A.append([int(x) for x in raw_input().split()])
A.sort(key=lambda x:x[1])
A.reverse()
ans=0
i=0
while n and i<len(A):
	k=min(n,A[i][0])
	ans+=A[i][1]*k
	n-=k
	i+=1
print ans
