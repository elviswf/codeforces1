#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#define MAXN 5002

using namespace std;
typedef pair<int, int> pii;

pii A[MAXN];
int ord[MAXN], DP[MAXN], pi[MAXN];
bool valid[MAXN];

bool order(const int &i, const int &j) {
  return A[i] < A[j];
}

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  cin >> A[0].first >> A[0].second;
  ord[0] = 0;
  for (int i = 1; i <= n; i++) {
    cin >> A[i].first >> A[i].second;
    ord[i] = i;
  }
  sort(ord+1, ord+n+1, order);
  memset(valid, 0, sizeof valid);
  valid[0] = 1;
  DP[0] = 0;
  int ans = 0;
  for (int i = 1; i <= n; i++) {
    DP[i] = 0;
    for (int j = 0; j < i; j++)
      if (A[ord[i]].first > A[ord[j]].first and
          A[ord[i]].second > A[ord[j]].second and
          valid[j] and DP[j] + 1 > DP[i]) {
        pi[i] = j;
        valid[i] = 1;
        DP[i] = DP[j] + 1;
        if (valid[i] and DP[i] > DP[ans])
          ans = i;
      }
  }
  vector<int> V;
  V.reserve(DP[ans]);
  while (ans) {
    V.push_back(ord[ans]);
    ans = pi[ans];
  }
  reverse(V.begin(), V.end());
  cout << V.size() << endl;
  if (V.size()) {
    cout << V[0];
    for (int i = 1; i < V.size(); i++)
      cout << " " << V[i];
    cout << endl;
  }
}
