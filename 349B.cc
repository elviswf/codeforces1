#include <iostream>
#include <cstring>
#define MAXV 1000001

using namespace std;

int M[10][MAXV];
int D[10][MAXV];
int A[10];

int DP(int dig, int v) {
  if (dig == 0)
    return 0;
  int &ans = M[dig][v];
  if (ans >= 0)
    return ans;
  ans = DP(dig-1, v);
  D[dig][v] = 0;
  if (A[dig] <= v) {
    int tmp = 1 + DP(dig, v - A[dig]);
    if (tmp >= ans) {
      ans = tmp;
      D[dig][v] = 1;
    }
  }
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  memset(M, -1, sizeof M);
  int v;
  cin >> v;
  for (int i = 1; i < 10; ++i)
    cin >> A[i];
  int n = DP(9, v);
  if (n) {
    int q = 0, dig = 9;
    while (q < n) {
      if (D[dig][v]) {
        v -= A[dig];
        cout << dig;
        ++q;
      } else {
        --dig;
      }
    }
  } else {
    cout << -1;
  }
  cout << endl;
}
