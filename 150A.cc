#include <iostream>
#include <vector>

using namespace std;
typedef long long ll;
typedef pair<int, int> pii;

int main() {
  ll q;
  cin >> q;
  vector<pii> A;
  int t, ac = 0;
  for (ll i = 2; i * i <= q; i++) {
    t = 0;
    while (q % i == 0) {
      t ++;
      q /= i;
    }
    if (t)
      A.push_back(pii(i, t));
    ac += t;
  }
  if (q > 1) {
    A.push_back(pii(q, 1));
    ac ++;
  }
  if (ac == 2) {
    cout << "2\n";
    return 0;
  }
  ac = 0;
  ll ans = 1;
  for (int i = 0; ac < 2 and i < A.size(); i++)
    for (int j = 0; ac < 2 and j < A[i].second; j++) {
      ans *= A[i].first;
      ac ++;
    }
  cout << "1\n";
  cout << (ac>1?ans:0) << endl;
}
