#include <iostream>
#include <bitset>
#define MOD 1000000007
#define MAXN 101

using namespace std;
typedef long long ll;

inline ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

char S[MAXN][MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; i++)
    cin >> S[i];
  int ans = 1;
  bitset<26> lett;
  for (int i = 0; i < m; i++) {
    lett.reset();
    for (int j = 0; j < n; j++)
      lett.set(S[j][i]-'A');
    ans = san((ll) ans * lett.count());
  }
  cout << ans << endl;
}
