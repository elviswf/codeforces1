#include <iostream>
#include <cstring>
#define MAXN 1000005

using namespace std;

int V[MAXN], loc[2*MAXN], n;

inline int& w(int x) {
  return loc[x + n + 1];
}

int main () {
  string S;
  cin >> S;
  n = S.size();
  memset(loc, -1, sizeof loc);
  w(0) = n;
  V[n] = 0;
  int x = 0;
  for (int i = n - 1; i >= 0; i--) {
    if (S[i] == ')') {
      ++ x;
      V[i] = 0;
    } else {
      -- x;
      V[i] = w(x) != -1 ? w(x) - i + V[w(x)] : 0;
    }
    w(x) = i;
  }
  int ans = 0;
  for (int i = 0; i < n; i++)
    ans = max(ans, V[i]);
  int howm;
  if (ans) {
    howm = 0;
    for (int i = 0; i < n; i++)
      howm += V[i] == ans;
  } else {
    howm = 1;
  }
  cout << ans << " " << howm << endl;
}
