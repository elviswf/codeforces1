#include <iostream>
#include <cstring>
#define MAXN 101
#define MOD 1000000007

using namespace std;
int k, d;

int M[MAXN][2];
/*
int DP(int n, bool f) {
  int &ans = M[n][f];
  if (ans >= 0)
    return ans;
  ans = 0;
  int limit = min(k+1, n+1);
  for (int i = 1; i < limit; ++i) {
    ans += DP(n-i, f or i >= d);
    if (ans >= MOD)
      ans -= MOD;
  }
  return ans;
}
*/

int main() {
  int n;
  cin >> n >> k >> d;
  //memset(M, -1, sizeof M);
  M[0][0] = 0;
  M[0][1] = 1;
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < 2; ++j) {
      for (int r = 1; r <= k and i - r >= 0; ++r) {
        M[i][j] += M[i-r][j or r >= d];
        if (M[i][j] >= MOD)
          M[i][j] -= MOD;
      }
    }
  }
  //cout << DP(n, 0) << endl;
  cout << M[n][0] << endl;
}
