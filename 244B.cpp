#include <iostream>
using namespace std;

typedef long long ll;

ll n, A[11];

ll dfs(ll x, int a, int b = -1) {
    if(x>n)
        return 0;
    return 1+dfs(x*10+a, a, b)+(b!=-1?dfs(x*10+b, a, b):0);
}

int main() {
    ll ans = 0;
    cin>>n;
    for(int i=1; i<10; i++)
        A[i] = dfs(i, i);
    for(int i=1; i<10; i++) {
        for(int j=i+1; j<10; j++)
            ans += dfs(i, i, j)+dfs(j, i, j) - A[i] - A[j];
        ans += dfs(i, i , 0);
    }
    cout<<ans<<endl;
}
