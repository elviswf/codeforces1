#include<cstdio>
#include<algorithm>
#include<iostream>

using namespace std;

struct obj
{
    long long pr;
    int id;
    bool t;
    obj(long long pr,bool t,int id)
    {
        this->pr=pr;
        this->t=t;
        this->id=id;
    }
    obj(){}
};

bool order(obj a,obj b)
{
    if(a.t==b.t)
        return a.pr>=b.pr;
    return a.t<b.t;
}

int main()
{
    int n,k;
    scanf("%d %d",&n,&k);
    obj A[1001];
    long long pr;
    int t;
    for(int i=0;i<n;i++)
    {
        cin>>pr;
        scanf("%d",&t);
        A[i]=obj(2LL*pr,t-1,i+1);
        //        cout<<"#"<<A[i].pr<<" "<<A[i].t<<" "<<A[i].id<<"\n";
    }
    //    puts("---------");
    sort(A,A+n,order);
    //    for(int i=0;i<n;i++)
    //        cout<<"#"<<A[i].pr<<" "<<A[i].t<<" "<<A[i].id<<"\n";
    //    puts("---------");
    int i=k;
    long long ans=0;
    for(int i=0;i<k-1;i++)
    {
        if(A[i].t)
            ans+=A[i].pr;
        else
            ans+=A[i].pr/2;
    }
    if(A[k-1].t)
        for(int i=k-1;i<n;i++)
            ans+=A[i].pr;
    else
    {
        long long mini=A[k-1].pr;
        for(int i=k-1;i<n;i++)
        {
            ans+=A[i].pr;
            mini=min(mini,A[i].pr);
        }
        ans-=mini/2;
    }
    cout<<(ans/2)<<"."<<(ans%2?5:0)<<"\n";
    for(int i=0;i<k-1;i++)
        printf("%d %d\n",1,A[i].id);
    printf("%d",n-k+1);
    for(int i=k-1;i<n;i++)
        printf(" %d",A[i].id);
    puts("");
}
