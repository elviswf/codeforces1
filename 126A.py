def ceil(num,den):
    if num%den == 0:
        return num/den
    return num/den+1

t1, t2, x1, x2, t0 = [int(x) for x in raw_input().split()]

if t1 == t0:
    if t1 == t2:
        print x1,x2
    else:
        print x1,0
    exit(0)
if t0 == t2:
    print 0,x2
    exit(0)

y1, y2 = 0, x2
tt = t2

for x in range(1,x1+1):
    y = ceil((t0-t1)*x,t2-t0)
    if y>x2:
        continue
    tx = float(t1*x+t2*y)/(x+y)
#    print '#',x,y,tx
    if tx<tt:
        tt = tx
        y1, y2 = x, y
    elif tx==tt:
        if x+y>y1+y2:
            y1, y2 = x, y

print y1,y2
