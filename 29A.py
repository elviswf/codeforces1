n=int(raw_input())
A=[]
for i in range(n):
	A.append([int(x) for x in raw_input().split()])

A.sort()

for i in range(n-1):
	for j in range(i+1,n):
		if A[i][0]+A[i][1]==A[j][0]:
			if A[j][0]+A[j][1]==A[i][0]:
				print 'YES'
				exit(0)
		elif A[i][0]+A[i][1]<A[j][0]:
			break
print 'NO'
