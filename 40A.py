from math import floor

s=raw_input().split()
x=int(s[0])
y=int(s[1])

rpta=0

if (x>=0 and y<0) or (x<0 and y>=0):
	rpta+=1

r=int(floor((x**2+y**2)**0.5))
if r**2==x**2+y**2 or (r+1)**2==x**2+y**2:
	print 'black'
	exit(0)
rpta+=r
if rpta%2:
	print 'white'
else:
	print 'black'
