s=raw_input().split()
n=int(s[0])
k=int(s[1])
isprime=[1]*(n+1)
prime=[]
for i in range(2,n+1):
	if(isprime[i]):
		prime.append(i)
		for j in range(i*2,n+1,i):
			isprime[j]=0

c=0
for i in prime[1:]:
	for j in range(len(prime)-1):
		if(prime[j]+prime[j+1]+1==i):
			c+=1
			break
	if(c==k):
		break
if(c>=k):
	print 'YES'
else:
	print 'NO'


