#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n;
  string s;
  cin >> n;
  cin >> s;
  int ans = count(s.begin(), s.end(), 'I');
  if (ans > 1)
    ans = 0;
  else if (ans == 0)
    ans = count(s.begin(), s.end(), 'A');
  cout << ans << endl;
}
