#include <iostream>
#include <cstring>

using namespace std;

int main() {
    int k;
    string s;
    int A[26];
    memset(A, 0, sizeof A);
    cin>>k>>s;
    for(int i=0; i<s.size(); i++)
        A[s[i]-'a']++;
    string ans;
    for(int i=0; i<26; i++) {
        if(A[i]%k==0) {
            ans += string(A[i]/k, 'a'+i);
        }
        else {
            cout<<-1<<endl;
            return 0;
        }
    }
    for(int i=0; i<k; i++)
        cout<<ans;
    cout<<endl;
}
