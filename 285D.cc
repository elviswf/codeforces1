#include <iostream>
#include <map>
#define MOD 1000000007

using namespace std;
typedef long long ll;
typedef pair<int, int> pii;

#define get(mask, i) ((mask) & (1<<(i)))

inline ll san(ll x, ll m = MOD) {
  if (x >= m or x < -m)
    x %= m;
  if (x < 0)
    x += m;
  return x;
}

ll fact(int n) {
  ll ans = 1;
  while (n)
    ans = san(ans * n--);
  return ans;
}

int n;
ll ans = 0;
map<pii, int> M[2];

inline int tog(int a, int b) {
  return san(a+b, n);
}

void bt(int a, int b, int m, int id) {
  if (id == (m ? n : n / 2 + 1)) {
    pii t(a, b);
    //cout << m << ": " << a << " " << b << endl;
    M[m][t] = san(M[m][t] + 1);
    return;
  }
  for (int i = 0; i < n; i++)
    if (not get(a, i) and not get(b, tog(id, i)))
      bt(a+(1<<i), b+(1<<tog(id,i)), m, id+1);
}

int main() {
  cin >> n;
  if (n & 1) {
    bt(1, 1, 0, 1);
    bt(0, 0, 1, n/2 + 1);
    int tot = (1<<n)-1;
    for(map<pii, int>::iterator it = M[0].begin(); it != M[0].end(); it++) {
      pii t(it->first);
      ans = san(ans + san(it->second *
            M[1][pii(tot^t.first, tot^t.second)]));
    }
  }
  cout << M[0].size() << " " << M[1].size() << " :  ";
  cout << ans << endl;
  ans = san(ans * n);
  cout << san(ans * fact(n)) << endl;
}
