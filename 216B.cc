#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 101

using namespace std;

int color[MAXN];
vector<int> adj[MAXN];

bool dfs(int u, bool c = 0) {
  if (color[u] >= 0)
    return color[u] != c;
  color[u] = c;
  bool ans = 0;
  for (auto &v : adj[u])
    ans |= dfs(v, 1-c);
  return ans;
}

int main(void) {
  ios::sync_with_stdio(false);
  int n, m, u, v;
  cin >> n >> m;
  while (m--) {
    cin >> u >> v;
    --u, --v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  memset(color, -1, sizeof color);
  int ans = 0;
  for (u = 0; u < n; ++u)
    if (color[u] == -1)
      ans += dfs(u);
  if (n - ans & 1)
    ++ans;
  cout << ans << endl;
  return 0;
}
