#include <iostream>
#include <cstring>
#define MAXN 105

using namespace std;

short convert(int x) {
  short ans = 0;
  for (int i = 0; i < 3; i++) {
    ans += (x % 10 != 0) << i;
    x /= 10;
  }
  return ans;
}

short B[MAXN], A[MAXN];
int M[MAXN][8], n;
bool go[MAXN][8];

int DP(int id, int mask) {
  if (id == n)
    return 0;
  int &ans = M[id][mask];
  if (ans >= 0)
    return ans;
  bool &g = go[id][mask];
  int tmp;
  ans = DP(id + 1, mask);
  if ((mask & A[id]) == 0) {
    tmp = 1 + DP(id+1, mask | A[id]);
    if (tmp > ans) {
      ans = tmp;
      g = 1;
    }
  }
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  int x;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> B[i];
    A[i] = convert(B[i]);
  }
  memset(M, -1, sizeof M);
  memset(go, 0, sizeof go);
  int ans = DP(0, 0);
  cout << ans << endl;
  bool first = true;
  int mask = 0;
  for (int id = 0; id < n; id++) {
    if (go[id][mask]) {
      if (not first)
        cout << " ";
      else
        first = false;
      cout << B[id];
      mask |= A[id];
    }
  }
  cout << endl;
  return 0;
}

