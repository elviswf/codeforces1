#include <vector>
#include <cmath>
#include <iostream>

using namespace std;

typedef int type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  double norm() const {
    return sqrt(norm2());
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;

void show(const pt &a) {
    cout << a.x << " " << a.y << endl;
}

bool isRight(vector<pt> &A) {
    for (int id = 0; id < 3; ++id) {
        pt d1 = A[(id+1)%3] - A[id],
           d2 = A[(id+2)%3] - A[id];
        int co = (d1*d2);
        if (d1 != pt(0, 0) and d2 != pt(0, 0) and co == 0)
            return true;
    }
    return false;
}

int main() {
    vector<pt> A(3);
    for (int i = 0; i < 3; ++i)
        cin >> A[i].x >> A[i].y;
    if (isRight(A)) {
        cout << "RIGHT" << endl;
        return 0;
    }
    int dx[] = {-1, 1, 0, 0},
        dy[] = {0, 0, -1, 1};
    for (int id = 0; id < 3; ++id) {
        for (int i = 0; i < 4; ++i) {
            vector<pt> B = A;
            B[id] += pt(dx[i], dy[i]);
            if (isRight(B)) {
                cout << "ALMOST" << endl;
                return 0;
            }
        }
    }
    cout << "NEITHER" << endl;
}
