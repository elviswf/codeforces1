#include <queue>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

typedef pair<int, int> pii;
typedef pair<int, pii> ppii;
const int INF = 0x7fffffff;

int dx[] = {1, -1, 0, 0}, dy[] = {0, 0, 1, -1}, n, m, k;
char T[101][101];
int d[101][101], xx, yy;
pii pi[101][101];

inline int val(char c) {
    if(c>='0' and c<='9')
        return c-'0';
    return 1;
}

int dijkstra(int x, int y, char c) {
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++) {
            d[i][j] = INF;
        }
    queue<pii> Q;
    Q.push(pii(x, y));
    d[x][y] = 0;
    while(!Q.empty()) {
        pii u = Q.front(), v;
        Q.pop();
        if(T[u.first][u.second] == c) {
            xx = u.first;
            yy = u.second;
            return d[xx][yy];
        }
        int cost = val(T[u.first][u.second]);
        for(int i=0; i<4; i++) {
            v = pii(u.first+dx[i], u.second+dy[i]);
            if(v.first>=0 and v.first<n and v.second>=0 and v.second<m and
                    T[v.first][v.second]!='#' and
                    d[v.first][v.second]==INF) {
                d[v.first][v.second] = d[u.first][u.second]+cost;
                pi[v.first][v.second] = u;
                Q.push(v);
            }
        }
    }
    return INF;
}

vector<pii> path;

void buildpath(int x1, int y1, int x2, int y2) {
    path.clear();
    pii tmp;
    while(x2!=x1 or y2!=y1) {
        path.push_back(pii(x2, y2));
        tmp = pi[x2][y2];
        x2 = tmp.first;
        y2 = tmp.second;
    }
    reverse(path.begin(), path.end());
}

int main() {
    string walk;
    cin>>n>>m>>k;
    for(int i=0; i<n; i++)
        cin>>T[i];
    int x1, y1, x2, y2;
    cin>>x1>>y1>>walk>>x2>>y2;
    x1 --, y1 --, x2 --, y2 --;
    int d, id = 0, c;
    int rep = walk.size()+1;
    while(k and id<rep) {
        d = dijkstra(x1, y1, walk[id]);
        if(d<=k) {
            x1 = xx;
            y1 = yy;
            k -= d;
            id ++;
        }
        else {
            buildpath(x1, y1, xx, yy);
            for(int i=0; i<path.size(); i++) {
                if((c=val(T[x1][y1]))<=k) {
                    k -= c;
                    x1 = path[i].first;
                    y1 = path[i].second;
                }
                else
                    break;
            }
            break;
        }
        if(id == rep-2) {
            T[x2][y2] = '+';
            walk.push_back('+');
        }
    }
    cout<<x1+1<<" "<<y1+1<<endl;
}
