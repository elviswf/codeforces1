#include <iostream>
#include <vector>
#include <algorithm>
#define MAXN 100001

using namespace std;

typedef long double dd;
template <typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0): x(x), y(y) {}
  inline p2d operator - (const p2d &a) {
    return p2d(x-a.x, y-a.y);
  }
  inline dd operator % (const p2d &a) {
    return (dd)x * (dd)a.y - (dd)y * (dd)a.x;
  }
};
typedef long long ll;
typedef p2d<ll> pt;

pt T[MAXN];
int ini, fin;
inline void add(ll x, ll y) { // for increasing slope (x)
  pt p(x, y);
  while (fin - ini >= 2 and (p-T[fin-1]) % (p-T[fin-2]) < 0)
    fin --;
  T[fin++] = p;
}
inline ll eval(int i, ll x) {
  return T[i].x * x + T[i].y;
}
inline void remove(ll x) {
  while (fin - ini >= 2 and eval(ini, x) >= eval(ini+1, x))
    ini++;
}

ll A[MAXN], B[MAXN];
ll DP[MAXN];

int main() {
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 0; i < n; ++i)
    cin >> B[i];
  ini = fin = 0;
  for (int i = 0; i < n; ++i) {
    ll &res = DP[i];
    if (i == 0) {
      res = 0;
    } else {
      add(B[i-1], DP[i-1]);
      remove(A[i]);
      res = eval(ini, A[i]);
      //cout << i << ": " << res << " -> " << ini << " " << fin << endl;
    }
  }
  cout << DP[n-1] << endl;
}
