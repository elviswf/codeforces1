#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

bool issorted(vector<int> &A) {
    bool asc = true, desc = true;
    for(int i=1; i<A.size(); i++) {
        if(A[i-1]<A[i])
            desc = false;
        else if(A[i-1]>A[i])
            asc = false;
    }
    return desc or asc;
}

int main() {
    int n;
    cin>>n;
    vector<int> A(n);
    for(int i=0; i<n; i++)
        cin>>A[i];
    int gr = 1;
    for(int i=1; i<n; i++)
        gr += A[i] != A[i-1];
    if(gr<2) {
        puts("-1");
        return 0;
    }
    if(issorted(A)) {
        if(n==2) {
            puts("-1");
            return 0;
        }
        for(int i=1; i<n; i++)
            if(A[i]!=A[i-1]) {
                printf("%d %d\n", i, i+1);
                return 0;
            }
    }
    for(int i=1; i<n; i++) {
        if(A[i-1]!=A[i]) {
            swap(A[i-1], A[i]);
            if(not issorted(A)) {
                printf("%d %d\n", i, i+1);
                return 0;
            }
            swap(A[i-1], A[i]);
        }
    }
    puts("-1");
}
