#include <iostream>

using namespace std;

bool first = true;

void print(int x, int r) {
  if (r) {
    if (not first)
      cout << " ";
    cout << x;
    for (int i = 1; i < r; ++i)
      cout << " " << x;
    first = false;
  }
}

int main() {
  ios::sync_with_stdio(0);
  int n, k, l, r, sall, sk;
  cin >> n >> k >> l >> r >> sall >> sk;
  int snd = sk / k, frst = snd+1;
  print(frst, sk % k);
  print(snd, k - (sk % k));
  sk = sall - sk;
  k = n - k;
  if (k) {
    snd = sk / k, frst = snd+1;
    print(frst, sk % k);
    print(snd, k - (sk % k));
  }
  cout << endl;
}
