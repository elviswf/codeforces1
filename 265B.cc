#include <iostream>
using namespace std;
typedef long long ll;
ll A[100010];
inline ll abs(ll x) {
  return x>=0 ? x : -x;
}
int main() {
  int n;
  cin >> n;
  for(int i=0; i<n; i++)
    cin >> A[i];
  ll h = 0, ans = 0;
  for(int i=0; i<n; i++) {
    ans += abs(h-A[i]) + 1 + (bool)i;
    h = A[i];
  }
  cout << ans << endl;
}
