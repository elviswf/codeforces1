#include <iostream>
#include <algorithm>

using namespace std;

int A[101][101], n, m;

int eval(int x) {
  int ans = 0;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      ans += abs(A[i][j]-x);
  return ans;
}

int main() {
  int d;
  cin >> n >> m >> d;
  int mod = -1;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++) {
      cin >> A[i][j];
      if (mod != -1 and A[i][j] % d != mod) {
        cout << "-1\n";
        return 0;
      }
      mod = A[i][j] % d;
    }
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      A[i][j] = (A[i][j] - mod)/d;
  int lo = 1, hi = 10001, m1, m2, v1, v2;
  while (hi - lo > 2) {
    m1 = (2*lo+hi)/3, m2 = (lo+2*hi)/3;
    v1 = eval(m1), v2 = eval(m2);
    if (v1 < v2)
      hi = m2;
    else
      lo = m1;
  }
  v1 = min(eval(lo), eval(lo+1));
  cout << v1 << endl;
}
