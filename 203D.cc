#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

typedef long double type;

template<typename T>
struct p3d {
  T x, y, z;
  p3d(T x = 0, T y = 0, T z = 0) : x(x), y(y), z(z) {}
  p3d(const p3d &a) : x(a.x), y(a.y), z(a.z) {}
  p3d operator - (const p3d &a) const {
    return p3d(x-a.x, y-a.y, z-a.z);
  }
  p3d operator + (const p3d &a) const {
    return p3d(x+a.x, y+a.y, z+a.z);
  }
  p3d operator * (T k) const {
    return p3d(x*k, y*k, z*k);
  }
  p3d operator / (T k) const {
    return p3d(x/k, y/k, z/k);
  }
  p3d& operator -= (const p3d &a) {
    x -= a.x, y -= a.y, z -= a.z;
    return *this;
  }
  p3d& operator += (const p3d &a) {
    x += a.x, y += a.y, z += a.z;
    return *this;
  }
  p3d& operator *= (T k) {
    x *= k, y *= k, z *= k;
    return *this;
  }
  p3d& operator /= (T k) {
    x /= k, y /= k, z/= k;
    return *this;
  }
  bool operator == (const p3d &a) const {
    return x == a.x and y == a.y and z == a.z;
  }
  bool operator != (const p3d &a) const {
    return x != a.x or y != a.y or z != a.z;
  }
  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
    return sqrt(norm2());
  }
};
typedef p3d<type> pt;

int a, b;

pt hitdoor(const pt &P, const pt &v) {
  type t = -P.y / v.y;
  pt A = P + v * t;
  //cout << "# " << A.x << " " << A.y << " " << A.z << endl;
  if (A.x >= 0 and A.x <= a and A.z >= 0 and A.z <= b)
    return A;
  return pt(-1, -1, -1);
}

bool hitX(int x, pt &P, pt &v) {
  if (v.x == 0)
    return false;
  type t = (x - P.x) / v.x;
  if (t <= 0)
    return false;
  pt tmp = P + v * t;
  if (tmp.z < 0 or tmp.z > b)
    return false;
  P = tmp;
  v.x *= -1;
  return true;
}

bool hitZ(int z, pt &P, pt &v) {
  if (v.z == 0)
    return false;
  type t = (z - P.z) / v.z;
  if (t <= 0)
    return false;
  pt tmp = P + v * t;
  if (tmp.x < 0 or tmp.x > a)
    return false;
  P = tmp;
  v.z *= -1;
  return true;
}

int main() {
  int m;
  cin >> a >> b >> m;
  pt P(a/2., m, 0), v, ans;
  cin >> v.x >> v.y >> v.z;
  while ((ans=hitdoor(P, v)).y < -1e-10) {
    /*
    cout << P.x << " " << P.y << " " << P.z;
    cout << " -> " << v.x << " " << v.y << " " << v.z << endl;
    */
    if (hitX(0, P, v)) continue;
    if (hitX(a, P, v)) continue;
    if (hitZ(0, P, v)) continue;
    hitZ(b, P, v);
  }
  printf("%.7f %.7f\n", (double)ans.x, (double)ans.z);
}
