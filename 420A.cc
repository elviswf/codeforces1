#include <iostream>

using namespace std;

string M = "AHIMOTUVWXY";

int main() {
  string s;
  cin >> s;
  bool valid = true;
  for (int i = 0; valid and i <= (s.size()>>1); ++i)
    valid &= M.find(s[i]) != string::npos and s[i] == s[s.size()-i-1];
  cout << (valid ? "YES" : "NO") << endl;
}
