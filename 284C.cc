#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;
typedef long long ll;

#define MAXN 524288
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
ll st[MAXN], add[MAXN];
int n;

inline void push(int idx, int l, int r, int L, int R) {
  if (add[idx]) {
    st[idx] += add[idx] * (r-l+1);
    if (l != r) {
      add[L] += add[idx];
      add[R] += add[idx];
    }
    add[idx] = 0;
  }
}

void stupdate(int i, int j, ll up, int idx = 0, int l = 0, int r = n) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    add[idx] += up;
  push(idx, l, r, L, R);
  if ((l >= i and r <= j) or r < i or l > j)
    return;
  int mid = center(l, r);
  stupdate(i, j, up, L, l, mid);
  stupdate(i, j, up, R, mid + 1, r);
  st[idx] = st[L] + st[R];
}

ll stq(int i, int j, int idx = 0, int l = 0, int r = n) {
  int L = left(idx), R = L + 1;
  push(idx, l, r, L, R);
  if (l >= i and r <= j)
    return st[idx];
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return stq(i, j, L, l, mid) + stq(i, j, R, mid+1, r);
}

int main() {
  ios::sync_with_stdio(0);
  int t = 1;
  cin >> n;
  int op, a, x;
  memset(st, 0, sizeof st);
  memset(add, 0, sizeof add);
  for (int i = 0; i < n; i++) {
    cin >> op;
    if (op == 1) {
      cin >> a >> x;
      stupdate(0, a-1, x);
    } else if (op == 2) {
      cin >> x;
      stupdate(t, t, x);
      t ++;
    } else {
      t --;
      stupdate(t, t, -stq(t, t));
    }
    printf("%.07f\n", 1. * stq(0, t-1) / t);
  }
}
