#include <iostream>
#include <cstring>

using namespace std;

int next[1000001];

void criba() {
  memset(next, 0, sizeof next);
  next[0] = 1;
  next[1] = 1;
  for (int i = 2; i * i < 1000001; i++)
    if (not next[i])
      for (int j = i * i; j < 1000001; j+= i)
        next[j] = 1;
  for (int i = 1000000; i; i--)
    if (next[i])
      next[i] = 1 + next[i+1];
}

int A[600][600];

int main() {
  criba();
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++)
      cin >> A[i][j];
  int ans = 0xf777777, tmp;
  for (int i = 0; i < n; i++) {
    tmp = 0;
    for (int j = 0; j < m; j++)
      tmp += next[A[i][j]];
    ans = min(ans, tmp);
  }
  for (int j = 0; j < m; j++) {
    tmp = 0;
    for (int i = 0; i < n; i++)
      tmp += next[A[i][j]];
    ans = min(ans, tmp);
  }
  cout << ans << endl;
}
