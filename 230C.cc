#include <iostream>
#define MAXN 101
#define MAXM 10001

using namespace std;

bool A[MAXN][2*MAXM];
int izq[MAXN][MAXM], der[MAXN][MAXM];
int n, m;

int solve() {
  int c;
  for (int i = 0; i < n; i++) {
    for (c = 0; not A[i][m-c] and m-c; c++) {}
    if (c == m)
      return -1;
    izq[i][0] = c;
    for (int j = 1; j < m; j++)
      izq[i][j] = A[i][j] ? 0 : 1 + izq[i][j-1];
  }
  for (int i = 0; i < n; i++) {
    for (c = 0; not A[i][m-1+c] and m-c; c++) {}
    if ( c == m)
      return -1;
    der[i][m-1] = c;
    for (int j = m - 2; j >= 0; j--)
      der[i][j] = A[i][j] ? 0 : 1 + der[i][j+1];
  }
  int ans = 1 << 30, tmp;
  for (int j = 0; j < m; j++) {
    tmp = 0;
    for (int i = 0; i < n; i++)
      tmp += min(izq[i][j], der[i][j]);
    ans = min(tmp, ans);
  }
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n >> m;
  string s;
  for (int i = 0; i < n; i++) {
    cin >> s;
    for (int j = 0; j < m; j++)
      A[i][j] = s[j] == '1';
    for (int j =  m; j < 2*m; j++)
      A[i][j] = A[i][j-m];
  }
  cout << solve() << endl;
}
