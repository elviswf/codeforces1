#include <iostream>
#include <cstring>
#define MAXN 101

using namespace std;

int M[MAXN][2][2], n;
int A[MAXN], B[MAXN];

int DP(int pos, bool up, bool down) {
  if (pos == n)
    return up or down ? 1 << 30 : 0;
  int &ans = M[pos][up][down];
  if (ans >= 0)
    return ans;
  ans = DP(pos+1, up^A[pos], down^B[pos]);
  ans = min(ans, 1 + DP(pos+1, up^B[pos], down^A[pos]));
  return ans;
}

int main() {
  memset(M, -1, sizeof M);
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i] >> B[i];
    A[i] &= 1;
    B[i] &= 1;
  }
  int ans = DP(0, 0, 0);
  cout << (ans < (1<<30) ? ans : -1) << endl;
}
