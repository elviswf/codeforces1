#include <iostream>
#include <cstring>
#define MAXK 10 
#define MOD 1000000007

using namespace std;
typedef long long ll;

ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

int C[MAXK][MAXK], dp[MAXK][MAXK];

ll pow(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = san(r*a);
    e >>= 1;
    a = san(a*a);
  }
  return r;
}

void pre(int k) {
  C[0][0] = 1;
  for (int i = 1; i < k; i++) {
    C[i][0] = C[i][i] = 1;
    for (int j = 1; j < i; j++)
      C[i][j] = san(C[i-1][j] + C[i-1][j-1]);
  }
}

int main() {
  ios::sync_with_stdio(0);
  int n, k;
  cin >> n >> k;
  pre(k);
  memset(dp, 0, sizeof dp);
  for (int an = 1; an <= k; an++)
    dp[0][an] = 1;
  for (int q = 1; q < k; q++)
    for (int an = 1; an < k; an++) {
      int &res = dp[q][an];
      for (int i = 1; i <= q; i++)
        res = san(res + san(san((ll)C[q][i]*dp[q-i][i]) * pow(an, i)));
    }
  cout << san(k*san(dp[k-1][1]*pow(n-k, n-k))) << endl;
}
