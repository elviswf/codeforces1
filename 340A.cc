#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int x, y, a, b;
  cin >> x >> y >> a >> b;
  int mcd = x * y / __gcd(x, y);
  a = (a+mcd-1) / mcd;
  b = b / mcd;
  cout << b - a + 1 << endl;
}
