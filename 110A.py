def islucky(n):
	if n==0:
		return 0
	while n>0:
		if n%10!=7 and n%10!=4:
			return 0
		n/=10
	return 1

a=raw_input()
c=0
for i in a:
	if i=='4' or i=='7':
		c+=1
if islucky(c):
	print 'YES'
else:
	print 'NO'
