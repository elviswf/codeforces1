n,m=[int(x) for x in raw_input().split()]
T=[]
for i in range(n):
	T.append([int(x) for x in raw_input().split()])
a,b=[int(x) for x in raw_input().split()]
ans=1<<30
for i in range(n-a+1):
	for j in range(m-b+1):
		tmp=0
		for k in range(a):
			for r in range(b):
				tmp+=T[i+k][j+r]
		ans=min(tmp,ans)
a,b=b,a
for i in range(n-a+1):
	for j in range(m-b+1):
		tmp=0
		for k in range(a):
			for r in range(b):
				tmp+=T[i+k][j+r]
		ans=min(tmp,ans)

print ans
