#include<cstdio>
#include<bits/stl_algobase.h>

using std::min;

int main()
{
    int n,A[]={0,0,0,0,0},tmp;
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        scanf("%d",&tmp);
        A[tmp]++;
    }
    int ans=A[4];
    tmp=min(A[1],A[3]);
    ans+=tmp;
    A[1]-=tmp;
    A[3]-=tmp;
    ans+=A[3]+A[2]/2+A[1]/4;
    A[2]%=2;
    A[1]%=4;
    tmp=A[1]+2*A[2];
    if(tmp>4)
        ans+=2;
    else if(tmp>0)
        ans+=1;
    printf("%d\n",ans);
}
