#include <iostream>
#include <map>
#define MAXN 1001

using namespace std;

int A[MAXN];

int main() {
  int n, x, k, cnt = 0, x0 = 0;
  cin >> n >> k;
  map<int, int> M;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    x = A[i] - i * k;
    if (x > 0 and ++M[x] >= cnt) {
      cnt = M[x];
      x0 = x;
    }
  }
  if (x0 == 0) {
    x0 = 1;
    cnt = 0;
  }
  cout << n - cnt << endl;
  for (int i = 0; i < n; ++i) {
    if (A[i] > x0)
      cout << "- " << i + 1 << " " << A[i] - x0 << endl;
    else if (A[i] < x0)
      cout << "+ " << i + 1 << " " << x0 - A[i] << endl;
    x0 += k;
  }
}

