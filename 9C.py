n=int(raw_input())
L=len(str(n))
ans=0
i=0
while i<(1<<L):
	t=''
	for j in range(L):
		if i&(1<<j):
			t+='1'
		else:
			t+='0'
	tt=int(t)
	if(tt>0 and tt<=n):
		ans+=1
	i+=1
print ans
