n=int(raw_input())
A=[int(x) for x in raw_input().split()]
mini=0
for i in range(1,n):
	if A[i]<=A[mini]:
		mini=i
maxi=n-1
for i in range(n-1,-1,-1):
	if A[i]>=A[maxi]:
		maxi=i
ans=n-mini-1+maxi
if mini<maxi:
	ans-=1
print ans
