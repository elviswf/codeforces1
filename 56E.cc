#include <iostream>
#include <algorithm>
#define MAXN 100005
#define MAXK 18
using namespace std;

struct data {
  int x, h, id;
  int reach() const {
    return x + h;
  }
  bool operator < (const data &a) const {
    return x < a.x;
  }
};

data D[MAXN];
int DP[MAXN];
int RMQ[18][MAXN];
int P[MAXN], ans[MAXN];

int query(int i, int j) {
  int lg = 31 - __builtin_clz(j-i+1);
  int id1 = RMQ[lg][i],
      id2 = RMQ[lg][j-(1<<lg)+1];
  return D[id1].reach() > D[id2].reach() ? id1 : id2;
}

int main() {
  ios::sync_with_stdio(0);
  int n, x, h;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> x >> h;
    D[i] = {x, h, i};
  }
  sort(D, D+n);
  for (int i = 0; i < n; ++i) {
    RMQ[0][i] = i;
    P[i] = D[i].x;
  }
  int id1, id2;
  for (int k = 1; (1<<k) <= n; ++k) {
    for (int i = 0; i + (1<<k) <= n; ++i) {
      id1 = RMQ[k-1][i];
      id2 = RMQ[k-1][i+(1<<(k-1))];
      RMQ[k][i] = D[id1].reach() > D[id2].reach() ? id1 : id2;
    }
  }
  int j, jd;
  for (int i = n - 1; i >= 0; --i) {
    j = lower_bound(P+i, P+n, D[i].reach()) - P - 1;
    if (j > i) {
      //cout << "< " << i << " " << j << endl;
      jd = query(i, j);
      //cout << "> " << i << ": " << jd << endl;
      DP[i] = jd > i ? jd - i + DP[jd] : j-i+1;
    } else {
      DP[i] = 1;
    }
    //cout << "# " << DP[i] << endl;
  }
  for (int i = 0; i < n; ++i)
    ans[D[i].id] = DP[i];
  cout << ans[0];
  for (int i = 1; i < n; ++i)
    cout << " " << ans[i];
  cout << endl;
}
