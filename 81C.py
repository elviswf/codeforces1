n=int(raw_input())
s=raw_input().split()
a=int(s[0])
b=int(s[1])

A=[(int(x),i) for i,x in enumerate(raw_input().split())]

def imprime(a):
	for i in range(len(a)-1):
		print a[i],
	print a[len(a)-1]

def order(a,b):
	if a[0]==b[0]:
		if(a[1]>b[1]):
			return -1
		return 1
	if a[0]<b[0]:
		return -1
	return 1

if a==b:
	imprime([1]*a+[2]*b)

else:
	if a>b:
		A.sort()
		r=[2]*n
		for i in range(a):
			r[A[i][1]]=1
		imprime(r)
	else:
		A.sort(cmp=order)
		r=[1]*n
		for i in range(b):
			r[A[i][1]]=2
		imprime(r)
