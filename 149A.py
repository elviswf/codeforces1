k = int(raw_input())
A = [int(x) for x in raw_input().split()]
A.sort(reverse=True)

s = 0
c = 0
for x in A:
    if s >=k:
        break
    s += x
    c += 1

if s>=k:
    print c
else:
    print -1
