#include <iostream>
#include <vector>
#include <algorithm>
#define MAXN 202

using namespace std;
typedef pair<int, int> pii;

vector<int> A[MAXN];
pii P[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m, t;
  cin >> n >> m;
  fill(P, P+n, pii(-1, -1));
  for (int i = 0; i < m; i++) {
    cin >> t;
    A[i].resize(t);
    for (int j = 0; j < t; j++) {
      cin >> A[i][j];
      P[--A[i][j]] = pii(i, j);
    }
  }
  vector<pii> ans;
  int p = 0;
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < A[i].size(); j++, p++)
      if (P[p] != pii(i, j)) {
        if (P[p] != pii(-1, -1)) {
          int k;
          for (k = p + 1; P[k] != pii(-1, -1); k++) {}
          ans.push_back(pii(p, k));
          A[P[p].first][P[p].second] = k;
          P[k] = P[p];
        }
        ans.push_back(pii(A[i][j], p));
        P[A[i][j]] = pii(-1, -1);
      }
  }
  cout << ans.size() << endl;
  for (pii &x : ans)
    cout << x.first + 1 << " " << x.second + 1 << endl;
}

