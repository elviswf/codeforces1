#include <iostream>
#define MAXN 1005
#define MOD 1000000007

using namespace std;

typedef long long ll;
ll DP[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, d;
  ll ans = -1;
  cin >> n;
  for (int i = 1; i <= n; ++i) {
    cin >> d;
    DP[i] = ++ans;
    ans += DP[i] - DP[d] + 1;
    ans %= MOD;
  }
  //cerr << endl;
  ++ans;
  ans %= MOD;
  if (ans < 0)
    ans += MOD;
  cout << ans << endl;
}
