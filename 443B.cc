#include <iostream>

using namespace std;

string s;
int n, k;

bool valid(int ini, int t) {
  bool v = true;
  for (int i = 0; i < t and ini + i + t < n; ++i)
    v &= s[ini+i] == s[ini+i+t];
  return v;
}

int main() {
  int ans = 0;
  cin >> s >> k;
  n = s.size();
  if (k >= n) {
    ans = (k + n) & ~1;
  } else {
    ans = k << 1;
    for (int i = 0; i < n; ++i) {
      for (int t = 1; i + 2 * t <= n + k; ++t)
        if (valid(i, t))
          ans = max(ans, 2 * t);
    }
  }
  cout << ans << endl;
}

