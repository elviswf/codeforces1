#include <cstdio>
#include <list>
#include <cstring>

using namespace std;
#define foreach(it,c) for(typeof((c).begin()) it=(c).begin();it!=(c).end();++it)

struct tri{
    int a, b, c;
    tri(int x, int y, int z) {
        a = x;
        b = y;
        c = z;
    }
};

int n, A[71], R[71];
list<tri> T;

int cuantosNum(int j) {
    memset(A, 0, sizeof A);
    foreach(it, T)
        if(it->a==j or it->b==j or it->c==j) {
            A[it->a] ++;
            A[it->b] ++;
            A[it->c] ++;
        }
    int sum = 0;
    for(int i=1; i<=n; i++)
        sum += A[i];
    return sum-A[j];
}

int masTrip() {
    memset(R, 0, sizeof R);
    foreach(it, T) {
        R[it->a] ++;
        R[it->b] ++;
        R[it->c] ++;
    }
    int maxi = 1, cc = 0, ccc;
    for(int i=2; i<=n; i++) {
        //printf("@ %d %d %d: %d %d %d\n", maxi, R[maxi], cc, i, R[i], cuantosNum(i));
        if(R[i]>R[maxi])
            maxi = i;
        else if(R[i]==R[maxi] and (ccc=cuantosNum(i))>cc) {
            cc = ccc;
            maxi = i;
        }
    }
    return maxi;
}

void borra(int j) {
    list<tri>::iterator it, it2;
    for(it=T.begin(); it!=T.end();) {
        if(it->a==j or it->b==j or it->c==j)
            it = T.erase(it);
        else
            it++;
    }
}

int main() {
    freopen("art.in", "r", stdin);
    freopen("art.out", "w", stdout);
    scanf("%d", &n);
    for(int a=1; a<=n; a++)
        for(int b=a+1; b<=n; b++)
            for(int c=b+1; c<=n; c++)
                if(2*(b-a)==c-b)
                    T.push_back(tri(a, b, c));
    /*
    foreach(it, T)
        printf("# %2d %2d %2d\n", it->a, it->b, it->c);*/
    bool del[71];
    memset(del, 0, sizeof del);
    int cb = 0;
    while(not T.empty()) {
        int j = masTrip();
        //printf("> %d\n", j);
        del[j] = 1;
        borra(j);
        cb ++;
    }
    printf("%d\n", n-cb);
    bool prim = true;
    for(int i=1; i<=n; i++)
        if(not del[i]) {
            if(not prim)
                printf(" ");
            printf("%d", i);
            prim = false;
        }
    puts("");
}
