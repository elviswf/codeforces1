#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>
#define MAXN 2001

using namespace std;

int C[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    C[findSet(i)] = findSet(j);
}

vector<int> adj[MAXN];
int pi[MAXN][20], n, lv[MAXN], dist[MAXN];
int used[MAXN][MAXN];

void predfs(int u, int h = 0, int p = -1) {
  lv[u] = h;
  int v;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (v != p) {
      pi[v][0] = u;
      dist[v] = dist[u] + used[u][v];
      predfs(v, h+1, u);
    }
  }
}

void prelca(int root = 0) {
  memset(pi, -1, sizeof pi);
  predfs(root);
  for (int j = 1; (1<<j) < n; j++)
    for (int i = 0; i < n; i++)
      if (pi[i][j-1] != -1)
        pi[i][j] = pi[pi[i][j-1]][j-1];
}

int lca(int u, int v) {
  if (lv[u] < lv[v])
    swap(u, v);
  int lg;
  for (lg = 1; (1<<lg) <= lv[u]; lg++) {}
  lg --;
  for (int i = lg; i >= 0; i--)
    if (lv[u] - (1<<i) >= lv[v])
      u = pi[u][i];
  if (u == v)
    return u;
  for (int i = lg; i >= 0; i--)
    if (pi[u][i] != -1 and pi[u][i] != pi[v][i])
      u = pi[u][i], v = pi[v][i];
  return pi[u][0];
}

typedef pair<int, int> pii;
typedef pair<int, pii> piii;

int main() {
  int n, u, v;
  bool first = true;
  while (scanf("%d", &n) != EOF) {
    if (not first)
      puts("");
    memset(used, 0, sizeof used);
    first = false;
    vector<piii> E;
    int x;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        scanf("%d", &x);
        if (i < j)
          E.push_back(piii(x, pii(i, j)));
      }
      C[i] = i;
      adj[i].clear();
    }
    sort(E.begin(), E.end());
    for (auto &x: E) {
      u = x.second.first;
      v = x.second.second;
      if (findSet(u) != findSet(v)) {
        used[u][v] = used[v][u] = x.first;
        adj[u].push_back(v);
        adj[v].push_back(u);
      }
      unionSet(u, v);
    }
    dist[0] = 0;
    prelca();
    int less = -1, d, p;
    int i = 0;
    for (auto &x: E) {
      u = x.second.first;
      v = x.second.second;
      if (not used[u][v]) {
        p = lca(u, v);
        d = dist[u] + dist[v] - 2 * dist[p];
        if (d > x.first) {
          less = i;
          break;
        }
      }
      ++i;
    }
    for (auto &x: E) {
      u = x.second.first;
      v = x.second.second;
      if (not used[u][v]) {
        p = lca(u, v);
        d = dist[u] + dist[v] - 2 * dist[p];
        if (d > x.first) {
          less = i;
          break;
        }
      }
      ++i;
    }

    int ii, jj;
    for (int i = 0; i < n; ++i)
      for (int j = i + 1; j < n; ++j)
        if (used[i][j]) {
          printf("%d %d %d\n", i+1, j+1, used[i][j]);
          ii = i, jj = j;
        }
    if (less >= 0) {
      u = E[i].second.first;
      v = E[i].second.second;
      printf("%d %d %d\n", u+1, v+1, E[i].first);
    } else {
      printf("%d %d %d\n", ii+1, jj+1, used[ii][jj]);
    }

  }
}
