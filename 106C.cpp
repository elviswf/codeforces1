#include<cstdio>
#include<bits/stl_algobase.h>

using std::max;

int M[11][1001],n,a[1001],b[1001],c[1001],d[1001];

int dp(int pos,int har)
{
	if(pos==n)
		return 0;
	int &ans=M[pos][har];
	if(ans>=0)
		return ans;
	int q=0,este=0;
	ans=0;
	while(har>=0 and este<=a[pos])
	{
		ans=max(ans,d[pos]*q+dp(pos+1,har));
		q++;
		este+=b[pos];
		har-=c[pos];
	}
	return ans;
}

int main()
{
	int har,c0,d0;
	scanf("%d %d %d %d",&har,&n,&c0,&d0);
	for(int i=0;i<n;i++)
		scanf("%d %d %d %d",&a[i],&b[i],&c[i],&d[i]);
	int ans=0,q=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<=har;j++)
			M[i][j]=-1;
	while(har>=0)
	{
		ans=max(ans,d0*q+dp(0,har));
		q++;
		har-=c0;
	}
	printf("%d\n",ans);
}
