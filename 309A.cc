#include <algorithm>
#include <cstdio>
#define MAXN 2000005

using namespace std;

int A[MAXN];

int main() {
  int n, l, t;
  scanf("%d %d %d", &n, &l, &t);
  t *= 2;
  int v = t / l, *p;
  t %= l;
  double ans = (double)v * n * (n-1) / 4;
  for (int i = 0; i < n; i++)
    scanf("%d", A+i);
  for (int i = n; i < 2 * n; i++)
    A[i] = A[i-n] + l;
  for (int i = 0; i < n; i++) {
    p = upper_bound(A+i+1, A+2*n, A[i] + t);
    ans += (p-(A+i+1)) / 4.;
  }
  printf("%.7f\n", ans);
}
