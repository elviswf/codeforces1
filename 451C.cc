#include <iostream>

using namespace std;

typedef long long ll;

ll n, k, d1, d2;

bool test(int mask) {
  ll t1 = (mask & 1) ? d1 : -d1,
     t2 = (mask & 2) ? d2 : -d2,
     t = k - t1 - t2,
     x[3];
  if (t % 3)
    return false;
  x[0] = t / 3;
  x[1] = x[0] + t1;
  x[2] = x[0] + t2;
  ll sum = 0;
  for (int i = 0; i < 3; ++i) {
    if (x[i] < 0 or x[i] > n)
      return false;
    sum += n - x[i];
  }
  //cerr << "> " << x[0] << ' ' << x[1] << ' ' << x[2] << endl;
  return sum == 3*n - k;
}

int main() {
  std::ios::sync_with_stdio(false);
  int tc;
  cin >> tc;
  while (tc--) {
    cin >> n >> k >> d1 >> d2;
    bool poss = false;
    if (n % 3 == 0) {
      n /= 3;
      for (int i = 0; not poss and i < 4; ++i)
        poss |= test(i);
    }
    cout << (poss ? "yes" : "no") << endl;
  }
}
