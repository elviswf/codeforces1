s=raw_input().split()
n=int(s[0])
k=int(s[1])
A=[0]*(k+1)
for x in [int(j) for j in raw_input().split()]:
	A[x]+=1
r=0
while A[k]!=n:
	r+=1
	for i in range(k-1,0,-1):
		if(A[i]>0):
			A[i]-=1
			A[i+1]+=1
print r
