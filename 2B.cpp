#include<cstdio>
#include<map>
#include<iostream>
#include<algorithm>
#define MAX 1001

using namespace std;

int n,A[MAX][MAX],M[MAX][MAX];

int count(int x,int d)
{
    if(x==0)
        return 0;
    int ans=0;
    while(x%d==0)
    {
        ans++;
        x/=d;
    }
    return ans;
}

int DP(int div)
{
    M[0][0]=count(A[0][0],div);
    for(int i=1;i<n;i++)
    {
        M[0][i]=count(A[0][i],div)+M[0][i-1];
        M[i][0]=count(A[i][0],div)+M[i-1][0];
    }
    for(int i=1;i<n;i++)
        for(int j=1;j<n;j++)
            M[i][j]=count(A[i][j],div)+min(M[i][j-1],M[i-1][j]);
    return M[n-1][n-1];
}

string rec(int div)
{
    string ans;
    int x=n-1,y=n-1,q;
    while(x!=0 or y!=0)
    {
        if(x==0)
        {
            y--;
            ans.push_back('R');
        }
        else if(y==0)
        {
            x--;
            ans.push_back('D');
        }
        else
        {
            q=count(A[x][y],div);
            if(M[x][y]==q+M[x-1][y])
            {
                ans.push_back('D');
                x--;
            }
            else
            {
                ans.push_back('R');
                y--;
            }
        }
    }
    reverse(ans.begin(),ans.end());
    return ans;
}

int main()
{
    scanf("%d",&n);
    int zi = -1;
    for(int i=0;i<n;i++)
        for(int j=0;j<n;j++) {
            scanf("%d",&A[i][j]);
            if(A[i][j] == 0)
              zi = i;
        }
    int ans = 1 << 30;
    string sol;
    if (zi != -1) {
      ans = 1;
      sol = string(zi, 'D') + string(n-1, 'R') + string(n-zi-1, 'D');
    }
    for (int i = 0; i < n; i++)
      for (int j = 0; j < n; j++)
        if (A[i][j] == 0)
          A[i][j] = 10;
    int ans2 = DP(2);
    if (ans2 < ans) {
      ans = ans2;
      sol = rec(2);
    }
    ans2=DP(5);
    if(ans2<ans)
    {
        ans=ans2;
        sol=rec(5);
    }
    printf("%d\n",ans);
    cout<<sol;
    puts("");
}
