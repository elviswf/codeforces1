#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 205

using namespace std;

typedef pair<int, int> pii;
vector<int> adj[MAXN];
int d[MAXN], nil;
pii ed[MAXN];

int dfs(int u, int p = -1) {
  int ans = u, t, v;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (v != p and v != nil) {
      d[v] = d[u] + 1;
      t = dfs(v, u);
      if (d[t] > d[ans])
        ans = t;
    }
  }
  return ans;
}

int diam(int root) {
  memset(d, 0, sizeof d);
  int u = dfs(root);
  memset(d, 0, sizeof d);
  return d[dfs(u)];
}

int main() {
  int n, u, v;
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n - 1; i++) {
    cin >> u >> v;
    u --, v--;
    adj[u].push_back(v);
    adj[v].push_back(u);
    ed[i] = pii(u, v);
  }
  int ans = 0, d1, d2;
  for (int i = 0; i < n - 1; i++) {
    nil = ed[i].second;
    d1 = diam(ed[i].first);
    nil = ed[i].first;
    d2 = diam(ed[i].second);
    ans = max(ans, d1*d2);
  }
  cout << ans << endl;
}
