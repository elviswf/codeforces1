#include <iostream>
#include <cstring>

using namespace std;

string s;
int m;
char M[1001][11][11];
int P[1001][11][11];

bool DP(int id, int dif, int last) {
  if (id == m)
    return true;
  char &ans = M[id][dif][last];
  if (ans >= 0)
    return ans;
  ans = false;
  for (int i = dif+1; not ans and i < 11; ++i)
    if (s[i-1] and i != last) {
      ans |= DP(id+1, i-dif, i);
      P[id][dif][last] = i;
    }
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> s >> m;
  for (int i = 0; i < s.size(); ++i)
    s[i] -= '0';
  memset(M, -1, sizeof M);
  bool ans = DP(0, 0, 0);
  cout << (ans ? "YES" : "NO") << endl;
  if (ans) {
    int dif, last;
    dif = last = P[0][0][0];
    cout << last;
    for (int id = 1; id < m; ++id) {
      last = P[id][dif][last];
      dif = last-dif;
      cout << " " << last;
    }
    cout << endl;
  }
}
