#include <iostream>
#include <cstring>
#define MAXN 100005

using namespace std;

int A[MAXN];

int main(void) {
  std::ios::sync_with_stdio(false);
  int n, x, k;
  cin >> n;
  bool valid = true;
  memset(A, -1, sizeof A);
  for (int i = 0; i < n; ++i) {
    cin >> x >> k;
    valid &= x <= A[k] + 1;
    A[k] = max(A[k], x);
  }
  cout << (valid ? "YES" : "NO") << endl;
  return 0;
}
