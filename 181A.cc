#include <iostream>
#include <vector>

using namespace std;
typedef pair<int, int> pii;

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  string s;
  vector<pii> A;
  for (int i = 0; i < n; i++) {
    cin >> s;
    for (int j = 0; j < m; j++)
      if (s[j] == '*')
        A.push_back(pii(i, j));
  }
  pii ans;
  if (A[0].first == A[1].first) {
    if (A[2].second == A[0].second)
      ans = pii(A[2].first, A[1].second);
    else
      ans = pii(A[2].first, A[0].second);
  } else {
    if (A[0].second == A[1].second)
      ans = pii(A[0].first, A[2].second);
    else
      ans = pii(A[0].first, A[1].second);
  }
  cout << ans.first + 1 << " " << ans.second + 1 << endl;
}
