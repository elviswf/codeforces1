#include <iostream>
#include <cstring>
#include <algorithm>
#define MAXN 100003

using namespace std;
int A[MAXN];

int main() {
  int n, m;
  cin >> n >> m;
  memset(A, 0, sizeof A);
  int c, t;
  for (int i = 1; i <= n; i++) {
    cin >> c >> t;
    A[i] = A[i-1] + c * t;
  }
  int *bg = A, *end = A + n;
  while (m--) {
    cin >> t;
    t--;
    bg = upper_bound(bg, end, t);
    cout << (bg - A) << endl;
  }
}
