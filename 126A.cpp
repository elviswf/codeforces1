#include<iostream>
#include<algorithm>

using namespace std;

long long ceil(long long num,long long den) {
    if(num%den==0)
        return num/den;
    return num/den+1;
}

void print(int a,int b) {
    cout<<a<<" "<<b<<"\n";
}

int main() {
    int t1,t2,x1,x2,t0;
    cin>>t1>>t2>>x1>>x2>>t0;
    if(t1==t0) {
        if(t1==t2)
            print(x1,x2);
        else
            print(x1,0);
        return 0;
    }
    if(t0==t2) {
        print(0,x2);
        return 0;
    }

    int y1=0,y2=x2;
    long long tn=t2,td=1;
    for(int x=1;x<=x1;x++) {
        long long y = ceil((t0-t1)*(long long)x,t2-t0);
        if(y>x2)
            continue;
        long long txn = (long long)t1*x+(long long)t2*y;
        long long txd = x+y;
        long long g = __gcd(txn,txd);
        txn/=g;
        txd/=g;
//        cout<<"# "<<x<<" "<<y<<" "<<tx<<endl;
        if(txn*td<txd*tn) {
            tn = txn;
            td = txd;
            y1 = x;
            y2 = y;
        }
        else if(txn*td==txd*tn)
            if(x+y>y1+y2) {
                y1 = x;
                y2 = y;
            }
    }
    print(y1,y2);
}
