#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;
typedef long long ll;

string s;

ll pow(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r *= a;
    e >>= 1;
    a *= a;
  }
  return r;
}

ll M[10][20][2];

ll dp(int ini, int p, bool aj) {
  ll &ans = M[ini][p][aj];
  if (not aj)
    return ans = pow(10, s.size()-p-1);
  if (p == s.size()-1)
    return ans = (ini <= s[s.size()-1]);
  ans = 0;
  for (int i = 0; i <= s[p]; i++)
    ans += dp(ini, p+1, i == s[p]);
  return ans;
}

ll go(ll x) {
  if (x < 10)
    return x;
  stringstream ss;
  ss << x;
  ss >> s;
  for (int i = 0; i < s.size(); i++)
    s[i] -= '0';
  memset(M, -1, sizeof M);
  ll ans = 0;
  for (int i = 1; i <= s[0]; i++)
    ans += dp(i, 1, i == s[0]);
  for (int i = 1; i < s.size(); i++)
    ans += 9 * pow(10, i > 1? i-2 : 0);
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  ll a, b;
  cin >> a >> b;
  cout << go(b) - go(a-1) << endl;
}
