#include <iostream>
#include <algorithm>

using namespace std;

int A[105], C[105];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  sort(A, A+n);
  for (int i = 0; i < n; ++i) {
    int j;
    for (j = A[i]; j; --j)
      if (C[j])
        break;
    if (j)
      C[j]--;
    C[j+1]++;
  }
  int ans = 0;
  for (int i = 0; i <= 100; ++i)
    ans += C[i];
  cout << ans << endl;
}
