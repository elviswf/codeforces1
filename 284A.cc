#include <iostream>

using namespace std;

int main() {
  int p, ans = 0;
  cin >> p;
  for (int x = 1; x < p; x++) {
    bool ispr = true;
    int xx = x;
    for (int i = 1; ispr and i < p-1; i++) {
      if (xx - 1 % p == 0)
        ispr = false;
      xx *= x;
      if (xx > p)
        xx %= p;
    }
    if (xx - 1 % p != 0)
      ispr = false;
    ans += ispr;
  }
  cout << ans << endl;
}
