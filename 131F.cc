#include <iostream>
#include <cstring>
#define MAXN 501

using namespace std;

typedef long long ll;
char A[MAXN][MAXN];
int B[MAXN][MAXN];
int C[MAXN];

int dx[] = {-1, 0, 1, 0},
    dy[] = {0, -1, 0, 1};

int main() {
  ios::sync_with_stdio(0);
  int n, m, q;
  cin >> n >> m >> q;
  for (int i = 0; i < n; ++i)
      cin >> A[i];
  int c;
  for (int i = 1; i < n - 1; ++i)
    for (int j = 1; j < m - 1; ++j) {
      if (A[i][j] == '1') {
        c = 0;
        for (int k = 0; k < 4; ++k)
          c += A[i+dx[k]][j+dy[k]] == '1';
        if (c == 4)
          B[i][j] = 1;
      }
    }
  ll ans = 0;
  int s, e;
  for (int i = 1; i < n - 1; ++i) {
    memset(C, 0, sizeof C);
    for (int j = i; j < n - 1; ++j) {
      for (int k = 1; k < m - 1; ++k)
        C[k] += B[j][k];
      c = 0;
      for (s = e = 1; s < m - 1; ++s) {
        while (e < m - 1 and c < q)
          c += C[e++];
        if (c >= q)
          ans += m - e;
        c -= C[s];
      }
    }
  }
  cout << ans << endl;
}

