#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int main() {
  int n, m;
  cin >> n >> m;
  vector<int> A(m);
  for (int i = 0; i < m; i++)
    cin >> A[i];
  priority_queue<int> Q(A.begin(), A.end());
  int emax = 0, t;
  for (int i = 0; i < n; i++) {
    t = Q.top(); Q.pop();
    emax += t;
    if (t-1)
      Q.push(t-1);
  }
  priority_queue<int, vector<int>, greater<int> >
      q(A.begin(), A.end());
  int emin = 0;
  for (int i = 0; i < n; i++) {
    t = q.top(); q.pop();
    emin += t;
    if (t-1)
      q.push(t-1);
  }
  cout << emax << " " << emin << endl;
}
