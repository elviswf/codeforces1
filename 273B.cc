#include <iostream>
#include <map>
#define MAXN 100002

using namespace std;
typedef long long ll;

int A[MAXN], B[MAXN], m;

ll san(ll x) {
  if (x >= m)
    x %= m;
  return x;
}

ll calc(int g, int r) {
  ll ans = 1;
  for (int i = 2; i <= g; i++)
    ans = san(ans * (r and i % 2 == 0 ? i / 2, r-- : i));
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  for (int i = 0; i < n; i++)
    cin >> B[i];
  cin >> m;
  map<int, int> G, R;
  for (int i = 0; i < n; i++) {
    G[A[i]] ++;
    G[B[i]] ++;
    if (A[i] == B[i])
      R[A[i]] ++;
  }
  ll ans = 1;
  for (map<int, int>::iterator it = G.begin(); it != G.end(); it++)
    ans = san(ans * calc(it->second,
          R.find(it->first) != R.end() ? R[it->first] : 0));
  cout << ans << endl;
}
