n = input()
if n < 3:
    print -1
else:
    ans = ''
    for i in range(n, 0, -1):
        ans += str(i) + ' '
    print ans[:-1]
